import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './login/login.component';
import { NeedAuthGuard } from './auth.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    loadChildren: './pages/pages.module#PagesModule',
    // redirectTo: '/copper',
    // pathMatch: 'full',
    canActivate: [NeedAuthGuard]
  },
  {
    path: '**',
    loadChildren: './pages/pages.module#PagesModule',
    // redirectTo: 'copper',
    // pathMatch: 'full',
    canActivate: [NeedAuthGuard]
  },
  // {
  //   path: "copper/wilayah/:wilayahs",
  //   loadChildren: './wilayah/wilayah.module#WilayahModule',
  //   canActivate: [NeedAuthGuard]
  // },

  // { 
  //   path: 'pages', 
  //   loadChildren: './pages/pages.module#PagesModule' 
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }