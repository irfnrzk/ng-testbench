import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router/src/router_state';
import { MatDialog } from '@angular/material';
// import { Location } from '@angular/common';
import { DialogAlertComponent } from './components/dialog-alert/dialog-alert.component';

@Injectable()
export class AccessNotice {

	constructor(public dialog: MatDialog) { }

	alert() {
		this.dialog.open(DialogAlertComponent, {
			panelClass: 'custom-dialog-container',
			width: '500px',
			data: {
				alertTitle: "Alert",
				alertNotice: "You are not authorized to view this page",
			}
		});
	}
}

@Injectable()
export class AccessGuardUser implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate() {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 1) {
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardSearch implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate() {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 2) {
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardWilayah implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate(routesnapshot: ActivatedRouteSnapshot) {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (Number(element.page_id) == 3) {
				if (Number(localStorage['usergroupid']) !== 9) {
					if (routesnapshot.url[2].path == localStorage['access_wilayah']) {
						return true;
					} else {
						this.accessNotice.alert();
						return false;
					}
				} else {
					return true;
				}
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardZone implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate(routesnapshot: ActivatedRouteSnapshot) {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (Number(element.page_id) == 4) {
				if (Number(localStorage['usergroupid']) !== 9) {
					if (routesnapshot.url[4].path == localStorage['access_zone']) {
						return true;
					} else {
						this.accessNotice.alert();
						return false;
					}
				} else {
					return true;
				}
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardNode implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate(routesnapshot: ActivatedRouteSnapshot) {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);
		var node_access = JSON.parse(localStorage['access_node']);

		if (Number(localStorage['usergroupid']) == 9) {
			return true
		} else {
			for (let i = 0; i < group_pageaccess.length; i++) {
				if (Number(group_pageaccess[i].page_id) == 5) {
					for (let j = 0; j < node_access.length; j++) {
						if (routesnapshot.url[6].path == node_access[j]) {
							return true;
						}
					}
				}
			}
			this.accessNotice.alert();
			return false;
		}
	}
}

@Injectable()
export class AccessGuardCabinet implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate(routesnapshot: ActivatedRouteSnapshot) {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);
		var cabinet_access = JSON.parse(localStorage['access_cabinet']);

		if (Number(localStorage['usergroupid']) == 9) {
			return true
		} else {
			for (let i = 0; i < group_pageaccess.length; i++) {
				if (Number(group_pageaccess[i].page_id) == 6) {
					for (let j = 0; j < cabinet_access.length; j++) {
						if (routesnapshot.url[8].path == cabinet_access[j]) {
							console.log(routesnapshot.url[8].path)
							console.log(cabinet_access[j])
							return true;
						}
					}
				}
			}
			this.accessNotice.alert();
			return false;
		}
	}
}

@Injectable()
export class AccessGuardDp implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate() {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 7) {
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardDpPair implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate() {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 8) {
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessGuardSystem implements CanActivate {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice) { }

	canActivate() {

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 10) {
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

@Injectable()
export class AccessLocked {

	constructor(public dialog: MatDialog, private accessNotice: AccessNotice, private route: ActivatedRoute, private router: Router) { }

	canDeactivate() {

		// console.log(localStorage['access_wilayah']);
		// console.log(localStorage['access_zone']);
		// var wilayah = this.route.snapshot.paramMap.get("");

		var group_pageaccess = JSON.parse(localStorage['group_pageaccess']);

		for (let index = 0; index < group_pageaccess.length; index++) {
			const element = group_pageaccess[index];
			if (element.page_id == 10) {
				// console.log(wilayah);
				// console.log(this.router.url);
				return true;
			}
		}
		this.accessNotice.alert();
		return false;
	}
}

