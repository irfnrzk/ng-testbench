import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { CustomerService } from '../customer.service';
import { ManagementService } from '../services/management.service';
import { concatMap, tap } from 'rxjs/operators';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isActive: boolean = false;
  isHidden: boolean = true;
  credentials: any = {};
  authResult: any = {};
  errMsg: string;

  userList: any;
  groupList: any;
  landpath: any;
  wilayahList: any;

  logindata: any = {};
  curDate = new Date();

  constructor(
    private router: Router,
    private dataservice: DataService,
    private customer: CustomerService,
    private managementservice: ManagementService,
    private searchservice: SearchService
  ) {
    this.credentials = {
      username: '', password: '', "apps": "RAM2.0"
    }
  }

  ngOnInit() {

    this.searchservice.getWilayahAll()
      .subscribe(res => {
        this.wilayahList = res['data'];
        localStorage['wilayah_list'] = JSON.stringify(this.wilayahList);
      }, error => {
        console.log("API Error");
      });

  }

  enterCreds() {
    this.isActive = true;
    this.isHidden = false;
  }

  tryLoginOip() {

    this.dataservice.loginOip(this.credentials)
      .pipe(tap(res => { this.authResult = res; }))
      .pipe(concatMap(() => this.managementservice.getGroup()))
      .pipe(tap(res => { this.groupList = res['data']; }))
      .pipe(concatMap(() => this.managementservice.getUserIndividual(this.credentials.username)))
      .pipe(tap(res => {

        if (this.authResult.result == 'AUTH_SUCCESS') {

          this.userList = res['data'];

          if (res['data_found'] == true) {

            if (this.userList[0].status_id == "1") {
              this.customer.setToken('token');
              localStorage['usergroupid'] = this.userList[0].group_id;

              //update user last login info
              let dateNow = `${this.curDate.getFullYear()}-${this.curDate.getMonth()}-${this.curDate.getDate()} ${this.curDate.getHours()}:${this.curDate.getMinutes()}:${this.curDate.getSeconds()}`;

              let dateUpdated: string;
              let datetemp = new Date(this.userList[0].updated);
              if (!isNaN(datetemp.getMonth())) {
                dateUpdated = `${datetemp.getUTCFullYear()}-${(datetemp.getUTCMonth() + 1)}-${datetemp.getUTCDate()} ${datetemp.getHours()}:${datetemp.getUTCMinutes()}:${datetemp.getUTCSeconds()}`;
              }
              else {
                dateUpdated = "0000-00-00 00:00:00"
              }

              this.logindata = {
                staff_id: this.credentials.username.toUpperCase(),
                last_login: dateNow,
                updated: dateUpdated
              };

              console.log(this.logindata);

            } else { this.errMsg = "Account is suspended. Please contact administrator."; }
          } else { this.errMsg = "Account does not exist"; }
        } else { this.errMsg = "Authentication failed"; }
      }))
      .pipe(concatMap(() => this.managementservice.updateLastLogin(this.logindata)))
      .subscribe(res => {
        let data = [];
        let nodetemp = [];
        let cabinettemp = [];
        let path: string;

        //build array of accessible pages
        this.groupList.map(a => {
          if (a.group_id == this.userList[0].group_id) {
            a.pages.map(b => {
              let group_pageid = b.page_id;
              let group_pagedesc = b.page_desc;
              data.push({ page_id: group_pageid, page_desc: group_pagedesc });
            })

            //control user access and landing page
            if (this.userList[0].district_id == null && a.group_id !== 9) {
              localStorage.removeItem('access_wilayah');
              localStorage.removeItem('access_zone');
              this.landpath = "/";
            } else if (this.userList[0].district_id == null && a.group_id == 9) {
              this.landpath = "/manageuser";
            } else {
              this.wilayahList.map(c => {
                c.zone.findIndex(element => {
                  if (element.description.includes(this.userList[0].district_name.toUpperCase()) == true) {

                    //build node list for user to access
                    element.node.map(d => {
                      const nodedata = d.slug;
                      nodetemp.push(nodedata);
                    })

                    //build cabinet list for user to access
                    element.node.map(node => {
                      node.cabinet.map(cab => {
                        const cabinet = cab.cab_id;
                        cabinettemp.push(cabinet);
                      });
                      node.msan.map(cab => {
                        const cabinet = cab.cab_id;
                        cabinettemp.push(cabinet);
                      });
                      node.rt.map(cab => {
                        const cabinet = cab.cab_id;
                        cabinettemp.push(cabinet);
                      });
                      node.dslam.map(cab => {
                        const cabinet = cab.cab_id;
                        cabinettemp.push(cabinet);
                      })
                    })

                    localStorage['access_wilayah'] = c.slug;
                    localStorage['access_zone'] = element.slug;
                    localStorage['access_node'] = JSON.stringify(nodetemp);
                    localStorage['access_cabinet'] = JSON.stringify(cabinettemp);

                    const wilayah = c.slug;
                    const zone = element.slug;

                    localStorage['group_pageaccess'] = JSON.stringify(data);

                    // assign land path
                    data.map(element => {
                      if (element.page_desc == a.default_page) {
                        switch (Number(element.page_id)) {
                          case 1:
                            path = "/management/user"
                            break;
                          case 2:
                            path = "/search"
                            break;
                          case 3:
                            path = "/copper/wilayah/" + wilayah
                            break;
                          case 4:
                            path = "/copper/wilayah/" + wilayah + "/zon/" + zone
                            break;
                          case 10:
                            path = "/management/poller"
                            break;
                          default:
                            path = "/"
                            break;
                        }
                        this.landpath = path;
                        return
                      }
                    });
                  }
                })
              })
            }
          }
        });

        this.router.navigateByUrl(this.landpath);
        // console.log(res.status);
        // this.router.navigateByUrl('');
      }, err => {
        console.log(err);
      });

  }

}