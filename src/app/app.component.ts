import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-testbench';

  isActive: boolean = false;
  isHidden: boolean = true;

  name = 'Angular 5';
  knOptions = {
    readOnly: true,
    size: 140,
    unit: '',
    textColor: '#757575',
    fontSize: '32',
    fontWeigth: '700',
    fontFamily: 'Roboto',
    valueformat: 'percent',
    value: 0,
    iconSize: 22,
    iconColor: '#cccccc',
    max: 1220,
    trackWidth: 9,
    barWidth: 6,
    trackColor: '#d4d4d4',
    barColor: '#4ea6d8',
    subText: {
      enabled: true,
      fontFamily: 'Verdana',
      font: '14',
      fontWeight: 'bold',
      text: 'Overall',
      color: '#000000',
      offset: 7
    },
  }
  value = 17;
  icon = 'user';

  constructor(private router: Router) {
  }

  enterCreds() {
    this.isActive = true;
    this.isHidden = false;
  }

  tryLoginOip() {
    this.router.navigate(['/pages']);
  }

}
