import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSettingsComponent, AddUserDialog, AddPageUserDialog } from './user-settings.component';
import { RouterModule, Routes } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { DialogAlertModule } from 'src/app/components/dialog-alert/dialog-alert.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatSelectModule, MatDialogModule, MatInputModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatAutocompleteModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: UserSettingsComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    DialogAlertModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    UserSettingsComponent,
    AddPageUserDialog,
    AddUserDialog
  ],
  entryComponents: [
    AddPageUserDialog,
    AddUserDialog
  ]
})
export class UserSettingsModule { }
