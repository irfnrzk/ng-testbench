import { Component, OnInit, ViewChild, ChangeDetectorRef, Inject, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { map, startWith, filter, distinct } from 'rxjs/operators';
import { ManagementService } from 'src/app/services/management.service';
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class UserSettingsComponent implements OnInit {

  user_list: any = [];
  dataSource: any = [];
  selection: any = [];
  selectedId = [];
  selectedValue = [];
  selectedName = [];
  group_list: any;
  staff_id: any;
  staff_name: any;
  staff_email: any;
  staff_status: any = "";
  staff_unit: any;
  staff_division: any;
  staff_group: any = "";
  staff_lastlogin: any;
  progressSpinner: boolean = false;
  group_staff: any;
  single_user: any;
  alertNotice: any = "";
  divisionSelect: boolean = true;
  tableAllPages: any;
  tableStaffPages: any;
  staff_district: any;
  district_temp: any;
  district_temp2: any;

  columnsToDisplay = ['select', 'staff_name', 'group_id', 'email', 'status_id', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private managementservice: ManagementService,
    private changeDetectorRefs: ChangeDetectorRef,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.managementservice.getGroup().subscribe(res => {
      this.group_list = res['data'].sort(function (a, b) {
        var groupA = a.group_name.toUpperCase(); // ignore upper and lowercase
        var groupB = b.group_name.toUpperCase(); // ignore upper and lowercase
        if (groupA < groupB) {
          return -1;
        }
        if (groupA > groupB) {
          return 1;
        }
        // names must be equal
        return 0;
      });
      this.group_staff = "9";
      this.getUser();
    }, error => {
      console.log("API Error");
    });
    // this.getUserAll();
  }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#manage-user-1',
  //         intro: help.manageuser[0],
  //         position: 'right'
  //       }]
  //   });
  //   this.introJS.start();
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selectGroup() {
    this.getUser();
    this.selectedId = [];
    this.selectedValue = [];
    this.selectedName = [];
  }

  getUser() {
    this.progressSpinner = true;
    this.managementservice.getUserGroup(this.group_staff).subscribe(res => {
      if (res['data_found'] = true) {
        this.progressSpinner = false;
      };
      this.user_list = res['data'];
      this.dataSource = new MatTableDataSource(this.user_list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.changeDetectorRefs.detectChanges();
    }, error => {
      console.log("API Error");
    });
  }

  getUserAll() {
    this.divisionSelect = false;
    this.progressSpinner = true;
    this.managementservice.getUser().subscribe(res => {
      if (res['data_found'] = true) {
        this.progressSpinner = false;
        this.user_list = res['data'];
        this.dataSource = new MatTableDataSource(this.user_list);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeDetectorRefs.detectChanges();
      };
    }, error => {
      console.log("API Error");
    });
  }

  id2Status(statusID: number) {
    if (statusID == 1) {
      return "Active";
    } else {
      return "Suspended";
    }
  }

  id2StatusAlt(statusID: number) {
    if (statusID == 1) {
      return "Suspend";
    } else {
      return "Activate";
    }
  }

  changeStatus(row: any) {
    var curDate = new Date();
    var dateNow = curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate() + ' ' + curDate.getHours() + ':' + curDate.getMinutes() + ':' + curDate.getSeconds();

    if (row.status_id == "1") {
      let newId = "2";
      let newStatus = {
        staff_id: row.staff_id,
        status_id: newId,
        updated: dateNow
      }
      this.managementservice.changeStatus(newStatus).subscribe(res => {
        this.alertNotice = row.staff_name + " is now suspended";
        this.alert();
        this.getUser();
        // this.getUserAll();
      }, err => {
        console.log("POST Error");
        return;
      });
    } else {
      let newId = "1";
      let newStatus = {
        staff_id: row.staff_id,
        status_id: newId,
        updated: dateNow
      }
      this.managementservice.changeStatus(newStatus).subscribe(res => {
        this.alertNotice = row.staff_name + " is now activated";
        this.alert();
        this.getUser();
        // this.getUserAll();
      }, err => {
        console.log("POST Error");
        return;
      });
    }
  }

  addAccess(row: any) {
    console.log(row.staff_pages);

    var groupaccess = JSON.parse(localStorage['group_pageaccess']);
    console.log(groupaccess);

    this.managementservice.getAccessPage().subscribe(res => {
      var allpages = res['data'];

      for (let i = 0; i < allpages.length; i++) {
        for (let j = 0; j < groupaccess.length; j++) {
          if (allpages[i] && allpages[i].page_id == groupaccess[j].page_id) {
            allpages.splice(i, 1);
          }
        }
      }

      for (let k = 0; k < allpages.length; k++) {
        for (let l = 0; l < row.staff_pages.length; l++) {
          if (allpages[k] && allpages[k].page_id == row.staff_pages[l].page_id) {
            allpages.splice(k, 1);
          }
        }
      }
      console.log(allpages);

      localStorage['staff_pages'] = JSON.stringify(row.staff_pages);
      localStorage['staff_allpages'] = JSON.stringify(allpages);
      this.tableAllPages = new MatTableDataSource(allpages);
      this.tableStaffPages = new MatTableDataSource(row.staff_pages);

      const dialogRef = this.dialog.open(AddPageUserDialog, {
        panelClass: 'custom-dialog-container',
        height: '560px',
        width: '500px',
        data: {
          staff_id: row.staff_id,
          allpages: allpages,
          staff_pages: row.staff_pages,
          tableallpages: this.tableAllPages,
          tablestaffpages: this.tableStaffPages
        }
      });

      dialogRef.componentInstance.dialogStaffPages.subscribe(result => {
        var staffPages = result;

        if (staffPages == null) {
        } else {
          var i = row.staff_pages.length;
          while (i--) {
            if (row.staff_pages[i] && row.staff_pages[i].page_id == staffPages.page_id) {
              row.staff_pages.splice(i, 1);
              allpages.push({ page_id: staffPages.page_id, page_desc: staffPages.page_desc });
            }
          }
          this.tableAllPages = new MatTableDataSource(allpages);
          this.tableStaffPages = new MatTableDataSource(row.staff_pages);

          dialogRef.componentInstance.data = {
            staff_id: row.staff_id,
            allpages: allpages,
            staff_pages: row.staff_pages,
            tableallpages: this.tableAllPages,
            tablestaffpages: this.tableStaffPages
          };
        }
      });

      dialogRef.componentInstance.dialogStaffAllPages.subscribe(result => {
        var staffAllPages = result;

        if (staffAllPages == null) {
        } else {
          var i = allpages.length;
          while (i--) {
            if (allpages[i] && allpages[i].page_id == staffAllPages.page_id) {
              allpages.splice(i, 1);
              row.staff_pages.push({ page_id: staffAllPages.page_id, page_desc: staffAllPages.page_desc });
            }
          }
          this.tableAllPages = new MatTableDataSource(allpages);
          this.tableStaffPages = new MatTableDataSource(row.staff_pages);

          dialogRef.componentInstance.data = {
            staff_id: row.staff_id,
            allpages: allpages,
            staff_pages: row.staff_pages,
            tableallpages: this.tableAllPages,
            tablestaffpages: this.tableStaffPages
          };
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == null) {
          return;
        } else {
          var pageinsert = [];
          for (let i = 0; i < result.staff_pages.length; i++) {
            pageinsert.push({ page_id: result.staff_pages[i].page_id })
          }
          var userpages = {
            staff_id: result.staff_id,
            page_ids: pageinsert
          };
          this.managementservice.addStaffPages(userpages).subscribe(res => {
            console.log(res.status);
            this.alertNotice = "Details for " + result.staff_name + " is updated";
            this.alert();
            this.getUser();
            // this.getUserAll();
          }, err => {
            console.log("POST Error");
            return;
          });
        }
      });
    }, error => {
      console.log("API Error");
    });
  }

  checkedUser(e, user) {
    // console.log(e.target.checked);
    if (e.target.checked === true) {
      this.selectedValue.push({ staff_id: user.staff_id });
      this.selectedId.push(user.staff_id);
      this.selectedName.push(user.staff_name);
    }
    else {
      var i = this.selectedId.length;
      while (i--) {
        if (this.selectedId[i] == user.staff_id) {
          this.selectedId.splice(i, 1);
          this.selectedValue.splice(i, 1);
          this.selectedName.splice(i, 1);
        }
      }
    }
    console.log(JSON.stringify(this.selectedId));
    // console.log(JSON.stringify(this.selectedValue));
    // console.log(JSON.stringify(this.selectedName));
  }

  editUser() {
    var curDate = new Date();
    var dateNow = curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate() + ' ' + curDate.getHours() + ':' + curDate.getMinutes() + ':' + curDate.getSeconds();
    this.checkedUser;

    if (this.selectedId.length == 1) {
      this.managementservice.getUserIndividual(this.selectedId[0]).subscribe(res => {
        this.single_user = res['data'];
        var datetemp = new Date(this.single_user[0].last_login);

        if (!isNaN(datetemp.getMonth())) {
          var dateApi = datetemp.getUTCFullYear() + '-' + (datetemp.getUTCMonth() + 1) + '-' + datetemp.getUTCDate() + ' ' + datetemp.getHours() + ':' + datetemp.getUTCMinutes() + ':' + datetemp.getUTCSeconds()
        }
        else {
          var dateApi = "0000-00-00 00:00:00"
        }

        const dialogRef = this.dialog.open(AddUserDialog, {
          panelClass: 'custom-dialog-container',
          height: '620px',
          width: '500px',
          data: {
            staff_id: this.single_user[0].staff_id,
            staff_name: this.single_user[0].staff_name,
            email: this.single_user[0].email,
            status_id: this.single_user[0].status_id.toString(),
            unit: this.single_user[0].unit,
            division: this.single_user[0].division,
            district: this.single_user[0].district_name,
            group_id: this.single_user[0].group_id.toString(),
            last_login: dateApi,
            addEditUser: "Edit",
          }
        });

        dialogRef.componentInstance.districtdata.subscribe(result => {
          this.district_temp = result;
        });

        dialogRef.afterClosed().subscribe(result => {

          if (result == null) {
            return;
          } else {
            if (result.district == this.single_user[0].district_name) {
              var temp = (this.single_user[0].district_id).toString();
              this.district_temp2 = temp;
            } else {
              var temp = (this.district_temp.district_id).toString();
              this.district_temp2 = temp;
            }

            var userdata = {
              staff_id: result.staff_id,
              staff_name: result.staff_name,
              email: result.email,
              status_id: result.status_id,
              unit: result.unit,
              division: result.division,
              group_id: result.group_id,
              district_id: this.district_temp2,
              updated: dateNow,
              last_login: result.last_login
            };

            this.managementservice.editUser(userdata).subscribe(res => {
              console.log(res.status);
              console.log(JSON.stringify(userdata));
              this.selectedId = [];
              this.selectedValue = [];
              this.selectedName = []
              this.alertNotice = "Details for " + result.staff_name + " is updated";
              this.alert();
              this.getUser();
              // this.getUserAll();
            }, err => {
              console.log("POST Error");
              return;
            });
          }
        });
      }, error => {
        console.log("API Error");
      });
    } else {
      this.alertNotice = "Please select one user";
      this.alert();
    }
  }

  deleteUser() {
    this.checkedUser;

    if (this.selectedId.length == 1) {
      var alertNotice = "Please confirm the deletion of user: " + this.selectedName;

      const dialogRef = this.dialog.open(DialogAlertComponent, {
        panelClass: 'custom-dialog-container',
        width: '600px',
        data: {
          staff_id: this.selectedValue,
          alertTitle: "Alert",
          alertNotice: alertNotice,
          confirmButton: true
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == null) {
          return;
        } else {
          let newdata = { staff_ids: result.staff_id };
          this.managementservice.deleteUser(newdata).subscribe(res => {
            // console.log(res.status);
            this.selectedId = [];
            this.selectedValue = [];
            this.selectedName = []
            this.getUser();
            // this.getUserAll();
          }, err => {
            console.log("POST Error");
            return;
          });
        }
      });
    } else {
      this.alertNotice = "Please select one user";
      this.alert();
    }
  }

  addUser() {
    var curDate = new Date();
    var dateNow = curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate() + ' ' + curDate.getHours() + ':' + curDate.getMinutes() + ':' + curDate.getSeconds();

    const dialogRef = this.dialog.open(AddUserDialog, {
      panelClass: 'custom-dialog-container',
      height: '620px',
      width: '500px',
      data: {
        staff_id: "",
        staff_name: "",
        email: "",
        status_id: "",
        unit: "",
        division: "",
        district: "",
        group_id: "",
        addEditUser: "Add"
      }
    });

    dialogRef.componentInstance.districtdata.subscribe(result => {
      this.district_temp = result;
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == null) {
        return;
      } else {
        let toTitleCase = (phrase) => {
          return phrase
            .toLowerCase()
            .split(' ')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1))
            .join(' ');
        };
        let staffname = toTitleCase(result.staff_name);

        let districttemp = (this.district_temp.district_id).toString();

        var userdata = {
          staff_id: result.staff_id.toUpperCase(),
          staff_name: staffname,
          email: result.email,
          status_id: result.status_id,
          unit: result.unit,
          division: result.division,
          district_id: districttemp,
          group_id: result.group_id,
          updated: dateNow,
          last_login: "",
        };

        this.managementservice.createUser(userdata).subscribe(res => {
          console.log(res.status);
          this.alertNotice = "New user created";
          this.alert();
          this.getUser();
          this.district_temp = null;
          // this.getUserAll();
        }, err => {
          console.log("POST Error");
          return;
        });
      }
    });
  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '500px',
      data: {
        alertTitle: "Alert",
        alertNotice: this.alertNotice,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}

@Component({
  selector: 'createuser-dialog',
  templateUrl: 'createuser-dialog.html',
  styleUrls: ['./user-settings.component.scss'],
})

export class AddUserDialog {
  group_list: any;
  districtControl = new FormControl();
  districtOptions: Observable<any[]>;
  district_list: any;

  @Output() districtdata = new EventEmitter<any>();

  constructor(
    private managementservice: ManagementService,
    public dialogRef: MatDialogRef<AddUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.managementservice.getGroup().subscribe(res => {
      this.group_list = res['data'].sort(function (a, b) {
        var groupA = a.group_name.toUpperCase(); // ignore upper and lowercase
        var groupB = b.group_name.toUpperCase(); // ignore upper and lowercase
        if (groupA < groupB) {
          return -1;
        }
        if (groupA > groupB) {
          return 1;
        }
        // names must be equal
        return 0;
      });

    }, error => {
      console.log("POST Error");
      return;
    });

    this.managementservice.getDistrictList().subscribe(res => {
      this.district_list = res['data'];
      this.districtFilter();
    }, error => {
      console.log("API Error");
    });
  }

  districtFilter() {
    this.districtOptions = this.districtControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this.districtFilterArray(value))
      );
  }

  districtFilterArray(value: any): string[] {
    if (value == undefined) {
      return
    } else {
      return this.district_list.map(x => x.district_name).filter(option => option.toLowerCase().includes(value.toLowerCase()));
    }
  }

  selectDistrict(option) {
    this.district_list.map(x => {
      if (x.district_name == option) {
        this.districtdata.emit(x)
      }
    })
  }

  cancelClicked(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'addpageuser-dialog',
  templateUrl: 'addpageuser-dialog.html',
  styleUrls: ['./user-settings.component.scss'],
})

export class AddPageUserDialog {

  columnsAccess: string[] = ['access_pages'];
  columnsAllPages: string[] = ['all_pages'];

  @Output() dialogStaffPages = new EventEmitter<any>();
  @Output() dialogStaffAllPages = new EventEmitter<any>();

  constructor(
    private managementservice: ManagementService,
    public dialogRef: MatDialogRef<AddPageUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  selectPages(row: any) {
    this.dialogStaffPages.emit(row);
  }

  selectAllPages(row: any) {
    this.dialogStaffAllPages.emit(row);
  }

  cancelClicked(): void {
    this.dialogRef.close();
  }
}
