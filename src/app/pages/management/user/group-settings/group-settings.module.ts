import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupSettingsComponent, AddGroupDialog } from './group-settings.component';
import { RouterModule, Routes } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { DialogAlertModule } from 'src/app/components/dialog-alert/dialog-alert.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatSelectModule, MatDialogModule, MatInputModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: GroupSettingsComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    DialogAlertModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    GroupSettingsComponent,
    AddGroupDialog
  ],
  entryComponents: [
    AddGroupDialog
  ]
})
export class GroupSettingsModule { }
