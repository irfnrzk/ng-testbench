import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { ManagementService } from 'src/app/services/management.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';

@Component({
  selector: 'app-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.scss']
})
export class GroupSettingsComponent implements OnInit {

  dataSource: any;
  dataSource2: any;
  group_list: any = [];
  group_id: any;
  group_name: any;
  group_level: any;
  group_desc: any;
  default_page: any;
  selectedId = [];
  selectedValue = [];
  selectedGroupName = [];
  progressSpinner: boolean = false;
  showLandingForm: boolean;
  alertNotice: any = "";

  columnsTable: string[] = ['select', 'group_name', 'group_level', 'groupdesc', 'default_page'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private managementservice: ManagementService, private changeDetectorRefs: ChangeDetectorRef, public dialog: MatDialog) { }

  ngOnInit() {
    this.getGroup();
    this.getAccessPage();
  }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#manage-group-1',
  //         intro: help.managegroup[0],
  //         position: 'right'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getGroup() {
    this.progressSpinner = true;
    this.managementservice.getGroup().subscribe(res => {
      if (res['data_found'] = true) {
        this.progressSpinner = false;
      }
      this.group_list = res['data'];
      this.dataSource = new MatTableDataSource(this.group_list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.changeDetectorRefs.detectChanges();
    }, error => {
      console.log("API Error");
    });
  }

  getAccessPage() {
    this.managementservice.getAccessPage().subscribe(res => {
      this.dataSource2 = res['data'];
    }, error => {
      console.log("API Error");
    });
  }

  addGroup() {
    const dialogRef = this.dialog.open(AddGroupDialog, {
      panelClass: 'custom-dialog-container',
      width: '500px',
      data: {
        group_name: "",
        group_level: "",
        group_desc: "",
        default_page: "",
        showLandingForm: "true",
        addEditGroup: "Add"
      }
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result == null) {
        return;
      } else {
        let toTitleCase = (phrase) => {
          return phrase
            .toLowerCase()
            .split(' ')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1))
            .join(' ');
        };
        let group_name = toTitleCase(result.group_name);

        var groupdata = {
          group_name: group_name,
          group_level: result.group_level,
          group_desc: result.group_desc,
          default_page: result.default_page
        };
        this.managementservice.createGroup(groupdata).subscribe(res => {
          // console.log(res.status);
          // alert("New group created");
          this.alertNotice = "New group created";
          this.alert();
          this.getGroup();
        }, err => {
          console.log("POST Error");
          return;
        });
      }
    });
  }

  checkedGroup(e, group) {
    // console.log(e.target.checked);

    if (e.target.checked === true) {
      this.selectedValue.push({ group_id: group.group_id });
      this.selectedId.push(group.group_id);
      this.selectedGroupName.push(group.group_name);
    }
    else {
      var i = this.selectedId.length;
      while (i--) {
        if (this.selectedId[i] == group.group_id) {
          this.selectedId.splice(i, 1);
          this.selectedValue.splice(i, 1);
          this.selectedGroupName.splice(i, 1);
        }
      }
    }
    // console.log(JSON.stringify(this.selectedId));
    // console.log(JSON.stringify(this.selectedValue));
    // console.log(JSON.stringify(this.selectedGroupName));
  }

  editGroup() {
    this.checkedGroup;
    console.log(this.selectedId);
    if (this.selectedId.length == 1) {
      for (let index = 0; index < this.group_list.length; index++) {
        const element = this.group_list[index];
        if (element.group_id == this.selectedId[0]) {
          const dialogRef = this.dialog.open(AddGroupDialog, {
            panelClass: 'custom-dialog-container',
            width: '500px',
            data: {
              group_id: this.group_list[index].group_id,
              group_name: this.group_list[index].group_name,
              group_level: this.group_list[index].group_level,
              group_desc: this.group_list[index].group_desc,
              default_page: this.group_list[index].default_page,
              addEditGroup: "Edit",
              hideLandingPage: true
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
              return;
            } else {
              var groupdata = {
                group_id: result.group_id,
                group_name: result.group_name,
                group_level: result.group_level,
                group_desc: result.group_desc
              };
              this.managementservice.editGroup(groupdata).subscribe(res => {
                // console.log(res.status);
                this.selectedId = [];
                this.selectedValue = [];
                this.selectedGroupName = [];
                // alert("Details for " + this.group_name + " is updated");
                this.alertNotice = "Details for " + result.group_name + " is updated";
                this.alert();
                this.getGroup();
              }, err => {
                console.log("POST Error");
                return;
              });
            }
          });
        }
      };
    } else {
      // alert("Please select one group");
      this.alertNotice = "Please select one group";
      this.alert();
    }
  }

  deleteGroup() {
    if (this.selectedId.length == 1) {
      this.checkedGroup;
      var alertNotice = "Please confirm the deletion of group: " + this.selectedGroupName;

      const dialogRef = this.dialog.open(DialogAlertComponent, {
        panelClass: 'custom-dialog-container',
        width: '500px',
        data: {
          group_id: this.selectedValue,
          alertTitle: "Alert",
          alertNotice: alertNotice,
          confirmButton: true
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == null) {
          return;
        } else {
          let newdata = { group_ids: result.group_id };
          this.managementservice.deleteGroup(newdata).subscribe(res => {
            // console.log(res.status);
            this.selectedId = [];
            this.selectedValue = [];
            this.selectedGroupName = [];
            this.getGroup();
            // this.getUserAll();
          }, err => {
            console.log("POST Error");
            return;
          });
        }
      });
    } else {
      this.alertNotice = "Please select one group";
      this.alert();
    }
  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '400px',
      data: {
        alertTitle: "Alert",
        alertNotice: this.alertNotice,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}

@Component({
  selector: 'creategroup-dialog',
  templateUrl: 'creategroup-dialog.html',
  // styleUrls: ['./managegroup.component.scss'],
})
export class AddGroupDialog {
  group_list: any;
  access_pages: any;

  @ViewChild('groupform') feedbackFormDirective;

  constructor(
    private managementservice: ManagementService,
    public dialogRef: MatDialogRef<AddGroupDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.managementservice.getGroup().subscribe(res => {
      this.group_list = res['data'];
    }, error => {
      console.log("API Error");
    });

    this.managementservice.getAccessPage().subscribe(res => {
      this.access_pages = res['data'];
    }, error => {
      console.log("API Error");
    });
  }

  cancelClicked(): void {
    this.dialogRef.close();
  }
}
