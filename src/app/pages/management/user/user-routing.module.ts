import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    canActivate: [NeedAuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'manage-user',
        pathMatch: 'full',
      },
      {
        path: 'manage-user',
        loadChildren: './user-settings/user-settings.module#UserSettingsModule'
      },
      {
        path: 'manage-group',
        loadChildren: './group-settings/group-settings.module#GroupSettingsModule'
      },
      {
        path: 'manage-link',
        loadChildren: './link-settings/link-settings.module#LinkSettingsModule'
      },
      {
        path: '**',
        redirectTo: 'user',
        pathMatch: 'full',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
