import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { ManagementService } from 'src/app/services/management.service';
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';

export interface DefaultPg {
  page_id: number;
  page_name: string;
  pagedesc: string;
}

@Component({
  selector: 'app-link-settings',
  templateUrl: './link-settings.component.html',
  styleUrls: ['./link-settings.component.scss']
})
export class LinkSettingsComponent implements OnInit {

  group_list: any;
  group_id: any;
  group_name: any;
  group_level: any;
  group_desc: any;
  dataSource: any;
  dataSource2: any;
  accessId: any = "";
  accessDesc: any = "";
  default_page: any = "";
  availableAccess: any = "";
  selectedOption: any = "";
  groupDefault: any;
  pageGroupAccess: any;
  pageIndex: any;
  accessibleTable: any;
  availableTable: any;
  accessRow: any;
  availableRow: any;
  selected: any;
  alertNotice: any = "";
  progressSpinner: boolean = false;
  labelLanding: boolean = false;
  disableLandingPage = new FormControl(true);

  allPages: any;
  groupPages: any;
  selectedGroup: any;
  leftRow: any;
  rightRow: any;
  defaultpage: any;
  selectedGroupPages: any;

  landpage = [
    {
      "page_id": "1",
      "page_desc": "User Settings"
    },
    {
      "page_id": "2",
      "page_desc": "Search"
    },
    {
      "page_id": "3",
      "page_desc": "Wilayah"
    },
    {
      "page_id": "4",
      "page_desc": "Zone"
    },
    {
      "page_id": "10",
      "page_desc": "System Management"
    }
  ];

  columnsGroup: string[] = ['group_name'];
  columnsAccess: string[] = ['access_pages'];
  columnsAllPages: string[] = ['all_pages'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private managementservice: ManagementService,
    private changeDetectorRefs: ChangeDetectorRef,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getGroup();
    this.getPageList();
  }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#management-1',
  //         intro: help.management[0],
  //         position: 'right'
  //       },
  //       {
  //         element: '#management-2',
  //         intro: help.management[1],
  //         position: 'right'
  //       },
  //       {
  //         element: '#management-3',
  //         intro: help.management[2],
  //         position: 'right'
  //       },
  //       {
  //         element: '#management-4',
  //         intro: help.management[3],
  //         position: 'right'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getGroup() {
    this.progressSpinner = true;
    this.managementservice.getGroup().subscribe(res => {
      this.group_list = res['data'];
      this.dataSource = new MatTableDataSource(this.group_list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.changeDetectorRefs.detectChanges();
      this.progressSpinner = false;
    }, error => {
      console.log("API Error");
    });
  }

  getPageList() {
    this.managementservice.getAccessPage().subscribe(res => {
      (localStorage['allpages']) = JSON.stringify(res['data']);
      this.dataSource2 = res['data'].sort(function (a, b) {
        var pageA = a.page_desc.toUpperCase(); // ignore upper and lowercase
        var pageB = b.page_desc.toUpperCase(); // ignore upper and lowercase
        if (pageA < pageB) {
          return -1;
        }
        if (pageA > pageB) {
          return 1;
        }
        // names must be equal
        return 0;
      });
    }, error => {
      console.log("API Error");
    });
  }

  onSelect(selectedItem: any) {
    this.selectedGroup = selectedItem;

    let pagetemp = selectedItem.pages;
    let landtemp = [];

    pagetemp.map(page => {
      this.landpage.map(land => {
        if (page.page_id == land.page_id) {
          landtemp.push(page);
        }
      })
    });

    // this.selectedGroupPages = selectedItem.pages;
    this.selectedGroupPages = landtemp;
    this.defaultpage = selectedItem.default_page;
    this.allPages = [];
    this.groupPages = [];
    var groupPages = [];
    var allPages = JSON.parse((localStorage['allpages']));
    for (let i = 0; i < this.selectedGroup.pages.length; i++) {
      groupPages.push(this.selectedGroup.pages[i]);
    }
    this.accessibleTable = new MatTableDataSource(groupPages);
    this.groupPages = groupPages;

    for (let j = 0; j < allPages.length; j++) {
      for (let k = 0; k < groupPages.length; k++) {
        if (allPages[j].page_id == Number(groupPages[k].page_id)) {
          allPages.splice(j, 1);
        }
      }
    }

    this.availableTable = new MatTableDataSource(allPages);
    this.allPages = allPages;
    this.labelLanding = true;
    this.disableLandingPage = new FormControl(false);
  }

  selectPages(row: any) {
    this.leftRow = row;
    this.rightRow = null;
  }

  selectAllPages(row: any) {
    this.rightRow = row;
    this.leftRow = null;
  }

  deleteLeft() {
    if (this.leftRow == null) {
      return
    } else {
      var groupPages = this.groupPages;
      var allPages = this.allPages;
      for (let i = 0; i < groupPages.length; i++) {
        if (groupPages[i].page_id == this.leftRow.page_id) {
          groupPages.splice(i, 1);
          allPages.push({ page_id: this.leftRow.page_id, page_desc: this.leftRow.page_desc });
        }
      }
      this.accessibleTable = new MatTableDataSource(groupPages);
      this.availableTable = new MatTableDataSource(allPages);
      groupPages = [];
      this.leftRow = null;
    }
  }

  delAllLeft() {
    var groupPages = this.groupPages;
    var allPages = this.allPages;
    for (let i = 0; i < groupPages.length; i++) {
      allPages.push({ page_id: groupPages[i].page_id, page_desc: groupPages[i].page_desc });
    }
    groupPages.splice(0);
    this.accessibleTable = new MatTableDataSource(groupPages);
    this.availableTable = new MatTableDataSource(allPages);
  }

  deleteRight() {
    if (this.rightRow == null) {
      return
    } else {
      var groupPages = this.groupPages;
      var allPages = this.allPages;
      for (let i = 0; i < allPages.length; i++) {
        if (allPages[i].page_id == this.rightRow.page_id) {
          allPages.splice(i, 1);
          groupPages.push({ page_id: this.rightRow.page_id, page_desc: this.rightRow.page_desc });
        }
      }
      this.accessibleTable = new MatTableDataSource(groupPages);
      this.availableTable = new MatTableDataSource(allPages);
      allPages = [];
      this.rightRow = null;
    }
  }

  delAllRight() {
    var groupPages = this.groupPages;
    var allPages = this.allPages;
    for (let i = 0; i < allPages.length; i++) {
      groupPages.push({ page_id: allPages[i].page_id, page_desc: allPages[i].page_desc });
    }
    allPages.splice(0);
    this.accessibleTable = new MatTableDataSource(groupPages);
    this.availableTable = new MatTableDataSource(allPages);
  }

  submitAccess() {
    if (this.selectedGroup == null) {
      return;
    } else {
      let newAccess = {
        group_id: this.selectedGroup.group_id,
        group_name: this.selectedGroup.group_name,
        group_level: this.selectedGroup.group_level,
        group_desc: this.selectedGroup.group_desc,
        default_page: this.selectedGroup.default_page,
        pages: this.accessibleTable.filteredData
      };

      let pagetemp = this.accessibleTable.filteredData;
      let landtemp = [];

      pagetemp.map(page => {
        this.landpage.map(land => {
          if (page.page_id == land.page_id) {
            landtemp.push(page);
          }
        })
      });

      this.selectedGroupPages = landtemp;
      this.managementservice.editAccess(newAccess).subscribe(res => {
        // console.log(res.status);
        this.alertNotice = "Pages access for group " + this.selectedGroup.group_name + " is updated";
        this.alert();
        this.getGroup();
      }, err => {
        if (err === 400) {
          console.log("The following parameter(s) is required : Group ID, Group Name, Group Level, Group Description, Default Page, & Page Info");
        } else {
          console.log("POST Error");
        }
        return;
      });
    }
  }

  submitDefPg() {
    let defpg = {
      group_id: this.selectedGroup.group_id,
      default_page: this.defaultpage,
    };

    this.managementservice.editDefaultPage(defpg).subscribe(res => {
      // console.log(res.status);
      this.alertNotice = "Landing page is updated";
      this.alert();
      this.getGroup();
    }, err => {
      if (err === 400) {
        console.log("The following parameter(s) is required : Group ID & Default Page");
      } else {
        console.log("POST Error");
      }
      return;
    });
  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '500px',
      data: {
        alertTitle: "Alert",
        alertNotice: this.alertNotice,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
