import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { SystemComponent } from './system.component';

const routes: Routes = [
  {
    path: '',
    component: SystemComponent,
    canActivate: [NeedAuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'scheduler',
        pathMatch: 'full',
      },
      {
        path: 'scheduler',
        loadChildren: './scheduler/scheduler.module#SchedulerModule'
      },
      {
        path: 'poller',
        loadChildren: './poller/poller.module#PollerModule'
      },
      {
        path: 'logtrail',
        loadChildren: './logtrail/logtrail.module#LogtrailModule'
      },
      {
        path: 'health',
        loadChildren: './health/health.module#HealthModule'
      },
      {
        path: '**',
        redirectTo: 'scheduler',
        pathMatch: 'full',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }