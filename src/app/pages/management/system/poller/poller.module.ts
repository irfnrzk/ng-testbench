import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PollerComponent } from './poller.component';
import { NeedAuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PollerComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    PollerComponent
  ]
})
export class PollerModule { }
