import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { LogtrailComponent } from './logtrail.component';

const routes: Routes = [
  {
    path: '',
    component: LogtrailComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    LogtrailComponent
  ]
})
export class LogtrailModule { }
