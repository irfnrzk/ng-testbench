import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogtrailComponent } from './logtrail.component';

describe('LogtrailComponent', () => {
  let component: LogtrailComponent;
  let fixture: ComponentFixture<LogtrailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogtrailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogtrailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
