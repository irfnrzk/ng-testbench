import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { NeedAuthGuard } from '../auth.guard';
import { AccessGuardUser, AccessGuardSearch, AccessGuardWilayah, AccessGuardZone, AccessGuardNode, AccessGuardCabinet, AccessGuardDp, AccessGuardDpPair } from "../accessguard";

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [NeedAuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'copper',
        pathMatch: 'full',
      },
      {
        path: 'copper',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: "copper/wilayah/:wilayahs",
        loadChildren: './home/wilayah/wilayah.module#WilayahModule',
        canActivate: [AccessGuardWilayah],
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones",
        loadChildren: './home/zon/zon.module#ZonModule',
        canActivate: [AccessGuardZone]
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes",
        loadChildren: './home/node/node.module#NodeModule',
        canActivate: [AccessGuardNode]
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes/cab/:cabs",
        loadChildren: './home/cabinet/cabinet.module#CabinetModule',
        canActivate: [AccessGuardCabinet]
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes/cabinfo/:cabinfos",
        loadChildren: './home/cabinet-info/cabinet-info.module#CabinetInfoModule',
        canActivate: [AccessGuardCabinet],
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes/cab/:cabs/dp/:dps",
        loadChildren: './home/network/network.module#NetworkModule',
        canActivate: [AccessGuardDp]
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes/cab/:cabs/netinfo/:netinfos",
        loadChildren: './home/network-info/network-info.module#NetworkInfoModule',
        canActivate: [AccessGuardDp]
      },
      {
        path: "copper/wilayah/:wilayahs/zon/:zones/node/:nodes/cab/:cabs/dp/:dps/serviceinfo/:serviceinfos",
        loadChildren: './home/service-info/service-info.module#ServiceInfoModule'
      },
      {
        path: 'management',
        loadChildren: './management/management.module#ManagementModule',
        canActivate: [AccessGuardUser]
      },
      {
        path: '**',
        redirectTo: 'copper',
        pathMatch: 'full',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
