import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { AccessGuardUser, AccessGuardSearch, AccessGuardWilayah, AccessGuardZone, AccessGuardNode, AccessGuardCabinet, AccessGuardDp, AccessGuardDpPair, AccessGuardSystem, AccessNotice } from "../accessguard";
import { MatDialogModule } from '@angular/material';
import { DialogAlertComponent } from '../components/dialog-alert/dialog-alert.component';
import { DialogAlertModule } from '../components/dialog-alert/dialog-alert.module';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatDialogModule,
    DialogAlertModule
  ],
  declarations: [
    PagesComponent
  ],
  providers: [
    AccessGuardUser,
    AccessGuardSearch,
    AccessGuardWilayah,
    AccessGuardZone,
    AccessGuardNode,
    AccessGuardCabinet,
    AccessGuardDp,
    AccessGuardDpPair,
    AccessGuardSystem,
    AccessNotice
  ]
})
export class PagesModule { }
