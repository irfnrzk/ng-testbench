import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CabinetInfoComponent } from './cabinet-info.component';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginator, MatSpinner, MatPaginatorModule, MatTabsModule, MatProgressSpinnerModule, MatDialogModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: CabinetInfoComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    NavpanelModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    CabinetInfoComponent
  ],
  exports: [
    CabinetInfoComponent
  ]
})
export class CabinetInfoModule { }
