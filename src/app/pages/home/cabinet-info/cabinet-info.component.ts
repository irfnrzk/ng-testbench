import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { DataService } from 'src/app/data.service';
import { CabinetinfoService } from 'src/app/services/cabinetinfo.service';
import { ActivatedRoute } from '@angular/router';
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';
import { tap, mergeMap, concatMap, switchMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cabinet-info',
  templateUrl: './cabinet-info.component.html',
  styleUrls: ['./cabinet-info.component.scss']
})
export class CabinetInfoComponent implements OnInit {

  // url param
  subscribedParam_w: any;
  subscribedParam_c: any;

  // shape data
  shape_id_data: any;

  // equipment info
  info_equipment: any;
  info_network: any;
  info_cabid: any;
  info_vendor: any;
  info_from: any;
  info_to: any;
  info_card_model: any;

  cable_condition: any;
  fault_no: any;
  str_last_update: any;
  cCond_check: any;
  cable_bundle_check: any;
  data_remark: any;

  cInfoArr: any = [];
  cBundleArr: any = [];
  cDetailsInfo: any;
  tValue: any; condition: any; ntt_history: any; cable_bundle: any; selectedCable: any;
  cCond: any; pCond: any; cableID: any; fCable: any; cCondAffDP: any; pCondAffDP: any;
  colorCond: string;
  nHistory: any;
  dataSourceCheck: any;
  showTable: boolean = false;
  cab_no: boolean = false;
  cab_yes: boolean = false;
  modalF: boolean = true;
  no_data: boolean = false;

  // spinner
  progressSpinner: boolean = false;

  // mat-table paginator
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private cabinetinfoService: CabinetinfoService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  dataSource;
  displayedColumns: string[] = [
    'date_time',
    'tt_no',
    'snr_up',
    'snr_down',
    'system_code',
    'remark',
    'attend'
  ];

  term$ = new Subject<string>();

  ngOnInit() {

    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_c = this.route.snapshot.paramMap.get("cabinfos");

    this.dataService.getElementView(this.subscribedParam_w)
      .pipe(tap(res => { this.shape_id_data = res['data'][0]; console.log(this.shape_id_data) }))
      .pipe(mergeMap(() => this.cabinetinfoService.cabinetInfo(this.subscribedParam_c)))
      .pipe(tap(res => {
        // console.log(this.selectedCable);
        if (res['data_found'] == false) {
          this.data_remark = "Data not available (Source : NIS and NEPS)";
          this.alert();
          this.no_data = true;
        } else {
          // load cabinet bundle and fixed info
          this.cable_bundle = res['data'][0].cable_code;
          this.cable_bundle_check = this.cable_bundle.length;
          this.info_equipment = res['data'][0].equipment;
          this.info_network = res['data'][0].network;
          this.info_cabid = res['data'][0].cabinet_id;
          this.info_vendor = res['data'][0].vendor;
          this.info_from = res['data'][0].from;
          this.info_to = res['data'][0].to;

          // load first cabinet bundle data
          if (this.cable_bundle_check > 0) {
            this.selectedCable = res['data'][0].cable_code[0].cable_code;
            this.selected(this.selectedCable);
          } else {
            this.data_remark = "Data not available (Source : NIS and NEPS)";
            this.alert();
            this.no_data = true;
          }
        }
      }))
      .subscribe();

    // this.cabinetinfoService.getNTT(this.subscribedParam_c).subscribe(responseList => {
    //   this.nHistory = responseList.data;
    //   this.dataSource = new MatTableDataSource(this.nHistory);
    //   this.dataSourceCheck = this.nHistory.length;
    //   if (this.dataSourceCheck > 0) {
    //     this.showTable = true;
    //   }
    //   this.dataSource.paginator = this.paginator;
    // });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // Trigger cabinet/cable bundle info
  selected(cable_id: string) {

    // this.progressSpinner = true;

    this.cabinetinfoService.getCabinetBundle(this.subscribedParam_c, cable_id).subscribe(res => {
      if (res['data_found'] = true) {
        this.progressSpinner = false;
      };

      // cable bundle selected info
      this.cable_condition = res.data[0].element_condition;
      this.cond(this.cable_condition);
      this.fault_no = res.data[0].fault;
      this.str_last_update = res.data[0].last_update;
      this.info_card_model = res.data[0].card_model;

      // if (res.data[0].cabinet_status == "False") {
      //   this.cab_no = true;
      // } else {
      //   this.cab_yes = true;
      // }

      // modal table
      // this.cCond = res.data[0].current_condition;
      // this.cCond_check = this.cCond.length;
      // if (this.cCond_check > 0) {
      //   this.cCondAffDP = res.data[0].current_condition[0].affected_dp;
      // }
    });
  }

  cond(cond: string) {
    switch (cond) {
      case "Stable":
        this.colorCond = 'text-green';
        break;
      case "Failure":
        this.colorCond = 'text-black';
        break;
      case "Caution":
        this.colorCond = 'text-yellow';
        break;
      case "Unstable":
        this.colorCond = 'text-red';
        break;
      case "N/A":
        this.colorCond = 'text-black';
        break;
      case null:
        this.colorCond = 'text-black';
        this.cable_condition = "N/A";
        break;
      case "nan":
        this.colorCond = 'text-black';
        this.cable_condition = "N/A";
        break;
    }
  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '400px',
      data: {
        alertTitle: "Error",
        alertNotice: this.data_remark,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // ngOnDestroy(): void {
  //   this.modalF = false;
  // }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#cabinet-info-1',
  //         intro: help.cabinetinfo[0],
  //         position: 'right'
  //       },
  //       {
  //         element: '#cabinet-info-2',
  //         intro: help.cabinetinfo[1],
  //         position: 'left'
  //       },
  //       {
  //         element: '#cabinet-info-3',
  //         intro: help.cabinetinfo[2],
  //         position: 'left'
  //       },
  //       {
  //         element: '#cabinet-info-4',
  //         intro: help.cabinetinfo[3],
  //         position: 'left'
  //       },
  //       {
  //         element: '#cabinet-info-5',
  //         intro: help.cabinetinfo[4],
  //         position: 'bottom'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }
}
