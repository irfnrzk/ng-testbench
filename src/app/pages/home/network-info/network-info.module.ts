import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginator, MatSpinner, MatPaginatorModule, MatTabsModule, MatProgressSpinnerModule, MatDialogModule } from '@angular/material';
import { NetworkInfoComponent } from './network-info.component';
import { NeedAuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: NetworkInfoComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    NavpanelModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    NetworkInfoComponent
  ],
  exports: [
    NetworkInfoComponent
  ]
})
export class NetworkInfoModule { }
