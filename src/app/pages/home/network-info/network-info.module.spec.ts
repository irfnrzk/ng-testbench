import { NetworkInfoModule } from './network-info.module';

describe('NetworkInfoModule', () => {
  let networkInfoModule: NetworkInfoModule;

  beforeEach(() => {
    networkInfoModule = new NetworkInfoModule();
  });

  it('should create an instance', () => {
    expect(networkInfoModule).toBeTruthy();
  });
});
