import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { NetworkinfoService } from 'src/app/services/networkinfo.service';

@Component({
  selector: 'app-network-info',
  templateUrl: './network-info.component.html',
  styleUrls: ['./network-info.component.scss']
})
export class NetworkInfoComponent implements OnInit {

  // url param
  subscribedParam_w: any;
  subscribedParam_z: any;
  subscribedParam_n: any;
  subscribedParam_c: any;
  subscribedParam_d: any;

  // shape data
  shape_id_data: any;

  // tab-links
  back_link_w: any;
  back_link_z: any;
  back_link_n: any;
  back_link_c: any;

  // equipment info
  info_condition: any;
  info_equipment: any;
  info_network: any;
  info_dpid: any;
  info_vendor: any;
  info_from: any;
  info_to: any;
  fault_no: any;
  str_last_update: any;
  Max_Attainable_Rate_up: any;
  Max_Attainable_Rate_dw: any;
  Attenuation_up: any;
  Attenuation_dw: any;
  SNR_Margin_up: any;
  SNR_Margin_dw: any;
  ntt_history: any;
  cCond: any;
  cCondAffDP: any;
  cCond_check: any;
  colorCond: string;
  eside: any;
  dside: any;
  info_card: any;

  data_remark: any;
  nHistory: any;
  dataSourceCheck: any;

  //spinner
  progressSpinner: boolean = false;
  showTable: boolean = false;
  cab_no: boolean = false;
  cab_yes: boolean = false;
  modalF: boolean = true;
  no_data: boolean = false;

  // mat-table paginator
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private networkinfoService: NetworkinfoService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) { }

  dataSource;
  displayedColumns: string[] = [
    'date_time',
    'tt_no',
    'snr_up',
    'snr_down',
    'system_code',
    'remark',
    'attend'
  ];

  returnToFault() {
    this.modalF = false;
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/dp/' + this.subscribedParam_d + '/fault/200');
  }

  ngOnInit() {

    this.subscribedParam_n = this.route.snapshot.paramMap.get('nodes');
    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_z = this.route.snapshot.paramMap.get("zones");
    this.subscribedParam_c = this.route.snapshot.paramMap.get("cabs");
    this.subscribedParam_d = this.route.snapshot.paramMap.get("netinfos");

    this.dataService.getElementView(this.subscribedParam_w).subscribe(res => {
      this.shape_id_data = res.data[0]; // shape_id      
    });

    this.progressSpinner = true;
    this.networkinfoService.networkInfo(this.subscribedParam_d).subscribe(responseList => {

      // load network details info
      if (responseList['data_found'] == false) {
        this.data_remark = responseList.data_remark;
        this.alert();
        this.progressSpinner = false;
        this.no_data = true;
      } else {
        this.progressSpinner = false;
        this.info_condition = responseList.data[0].element_condition;
        console.log(this.info_condition);
        this.cond(this.info_condition);
        this.info_equipment = responseList.data[0].equipment;
        this.info_card = responseList.data[0].dslam_card_model;
        this.info_dpid = responseList.data[0].dp_id;
        this.info_vendor = responseList.data[0].vendor;
        this.info_from = responseList.data[0].from;
        this.info_to = responseList.data[0].to;
        this.eside = responseList.data[0].eside_cable_code;
        this.dside = responseList.data[0].dside_cable_code;
        this.fault_no = responseList.data[0].current_condition.length;
        this.str_last_update = responseList.data[0].last_update;
        // this.Max_Attainable_Rate_up = responseList.data[0].AttainRateUs;
        // this.Max_Attainable_Rate_dw = responseList.data[0].AttainRateDs;
        // this.Attenuation_up = responseList.data[0].AttenuationUs;
        // this.Attenuation_dw = responseList.data[0].AttenuationDs;
        // this.SNR_Margin_up = responseList.data[0].SNRMarginUs;
        // this.SNR_Margin_dw = responseList.data[0].SNRMarginDs;
        this.ntt_history = responseList.data[0].ntt_history;

        //temp
        this.Max_Attainable_Rate_up = "N/A";
        this.Max_Attainable_Rate_dw = "N/A";
        this.Attenuation_up = "N/A";
        this.Attenuation_dw = "N/A";
        this.SNR_Margin_up = "N/A";
        this.SNR_Margin_dw = "N/A";

        // auto hide/show cabinet
        if (responseList.data[0].cabinet_status == "False") {
          this.cab_no = true;
        } else {
          this.cab_yes = true;
        }

        // modal table
        this.cCond = responseList.data[0].current_condition;
        this.cCond_check = this.cCond.length;
        if (this.cCond_check > 0) {
          this.cCondAffDP = responseList.data[0].current_condition[0].affected_service_number;
        }
      }

    });

    // this.networkinfoService.getNTT(this.subscribedParam_d).subscribe(responseList => {

    //   this.nHistory = responseList.data;
    //   this.dataSource = new MatTableDataSource(this.nHistory);
    //   this.dataSourceCheck = this.nHistory.length;
    //   if (this.dataSourceCheck > 0) {
    //     this.showTable = true;
    //   }
    //   this.dataSource.paginator = this.paginator;
    // });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#network-info-1',
  //         intro: help.networkinfo[0],
  //         position: 'right'
  //       },
  //       {
  //         element: '#network-info-2',
  //         intro: help.networkinfo[1],
  //         position: 'right'
  //       },
  //       {
  //         element: '#network-info-3',
  //         intro: help.networkinfo[2],
  //         position: 'right'
  //       },
  //       {
  //         element: '#network-info-4',
  //         intro: help.networkinfo[3],
  //         position: 'right'
  //       },
  //       {
  //         element: '#network-info-5',
  //         intro: help.networkinfo[4],
  //         position: 'right'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }

  ngOnDestroy(): void {
    this.modalF = false;
  }

  cond(cond) {

    switch (cond) {
      case "Stable":
        this.colorCond = 'text-green';
        break;
      case "Failure":
        this.colorCond = 'text-black';
        break;
      case "Caution":
        this.colorCond = 'text-yellow';
        break;
      case "Unstable":
        this.colorCond = 'text-red';
        break;
      case "N/A":
        this.colorCond = 'text-black';
        break;
      case null:
        this.colorCond = 'text-black';
        this.info_condition = "N/A";
        break;
      case "nan":
        this.colorCond = 'text-black';
        this.info_condition = "N/A";
        break;
    }

  }

  getLineTest() {

    this.progressSpinner = true;
    this.networkinfoService.networkInfoLT(this.subscribedParam_d).subscribe(res => {

      if (res['data_found'] = true) {
        this.progressSpinner = false;
      };

      // cable bundle selected info
      this.info_condition = res.data[0].element_condition;
      this.cond(this.info_condition);
      this.info_equipment = res.data[0].equipment;
      this.info_card = res.data[0].dslam_card_model;
      this.info_dpid = res.data[0].dp_id;
      this.info_vendor = res.data[0].vendor;
      this.info_from = res.data[0].from;
      this.info_to = res.data[0].to;
      this.eside = res.data[0].eside_cable_code;
      this.dside = res.data[0].dside_cable_code;
      this.fault_no = res.data[0].current_condition.length;
      this.str_last_update = res.data[0].last_update;
      // this.Max_Attainable_Rate_up = res.data[0].AttainRateUs;
      // this.Max_Attainable_Rate_dw = res.data[0].AttainRateDs;
      // this.Attenuation_up = res.data[0].AttenuationUs;
      // this.Attenuation_dw = res.data[0].AttenuationDs;
      // this.SNR_Margin_up = res.data[0].SNRMarginUs;
      // this.SNR_Margin_dw = res.data[0].SNRMarginDs;
      this.ntt_history = res.data[0].ntt_history;

      //temp
      this.Max_Attainable_Rate_up = "N/A";
      this.Max_Attainable_Rate_dw = "N/A";
      this.Attenuation_up = "N/A";
      this.Attenuation_dw = "N/A";
      this.SNR_Margin_up = "N/A";
      this.SNR_Margin_dw = "N/A";

      // modal table
      this.cCond = res.data[0].current_condition;
      this.cCond_check = this.cCond.length;
      if (this.cCond_check > 0) {
        this.cCondAffDP = res.data[0].current_condition[0].affected_service_number;
      }

    });

  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '400px',
      data: {
        alertTitle: "Error",
        alertNotice: this.data_remark,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
