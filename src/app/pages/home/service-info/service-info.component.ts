import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { DataService } from 'src/app/data.service';
import { ActivatedRoute } from '@angular/router';
import { ServiceinfoService } from 'src/app/services/service-info.service';
import { Chart } from "node_modules/chart.js/dist/Chart.bundle.min.js";
import { DialogAlertComponent } from 'src/app/components/dialog-alert/dialog-alert.component';

@Component({
  selector: 'app-service-info',
  templateUrl: './service-info.component.html',
  styleUrls: ['./service-info.component.scss']
})
export class ServiceInfoComponent implements OnInit {

  // url param
  subscribedParam_w: any;
  subscribedParam_e: any;

  // shape data
  shape_id_data: any;

  // equipment info
  info_condition: any;
  info_equipment: any;
  info_vendor: any;
  info_current_profile: any;
  info_curr_profile: any;
  info_service_id: any;
  info_prev_profile: any;
  info_previous_profile: any;
  info_unifi_id: any;
  info_pair_id: any;
  info_from: any;
  info_to: any;
  fault_no: any;
  str_last_update: any;
  Max_Attainable_Rate_up: any;
  Max_Attainable_Rate_dw: any;
  Attenuation_up: any;
  Attenuation_dw: any;
  SNR_Margin_up: any;
  SNR_Margin_dw: any;
  info_card_model: any;

  cCond: any;
  cCond_check: any;
  cCondAffDP: any;
  cHistory: any;
  condition: any;
  colorCond: string;
  pType: any;
  dataSourceCheck: any;
  data_remark: any;

  // chart
  chart: any;
  graph_val: any = [];
  ytitle: string;
  gTitle: string;
  dTitle1: string;
  dTitle2: string;
  notes: string;
  unit: string;

  progressSpinner: boolean = false;
  linechart: boolean = true;
  dailyT: boolean = false;
  dailyIrCr: boolean = false;
  isDisabled: boolean = true;
  showTable = false;
  cab_no = false;
  cab_yes = false;
  no_data = false;

  aGraphD: any; aGraphW: any; aGraphM: any; sGraphD: any; sGraphW: any; sGraphM: any; arGraphD: any; arGraphW: any; arGraphM: any;
  dateCurr: any; sUppCurr: any; sDwnCurr: any;

  sLabelsD: any = []; sUpCurrD: any = []; sDwCurrD: any = []; sLabelsW: any = []; sUpCurrW: any = []; sDwCurrW: any = []; sLabelsM: any = []; sUpCurrM: any = []; sDwCurrM: any = [];
  aLabelsD: any = []; aUpCurrD: any = []; aDwCurrD: any = []; aLabelsW: any = []; aUpCurrW: any = []; aDwCurrW: any = []; aLabelsM: any = []; aUpCurrM: any = []; aDwCurrM: any = [];
  arLabelsD: any = []; arUpCurrD: any = []; arDwCurrD: any = []; arLabelsW: any = []; arUpCurrW: any = []; arDwCurrW: any = []; arLabelsM: any = []; arUpCurrM: any = []; arDwCurrM: any = [];
  rag: any = []; rbg: any = [];
  irGraphD: any; irGraphW: any; irGraphM: any;
  crGraphD: any = []; crGraphW: any = []; crGraphM: any = [];
  irLabelsD: any = []; irRAB: any = []; irRAG: any = []; irRBG: any = []; rab: any = []; irLabelsW: any = []; irRABW: any = []; irRAGW: any = []; irRBGW: any = []; irLabelsM = []; irRABM = []; irRAGM = []; irRBGM = [];
  crLabelsD: any = []; crRAB: any = []; crRAG: any = []; crRBG: any = []; crLabelsW: any = []; crRABW: any = []; crRAGW: any = []; crRBGW: any = []; crLabelsM: any = []; crRABM: any = []; crRAGM: any = []; crRBGM: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private seviceinfoService: ServiceinfoService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  dataSource;
  displayedColumns: string[] = [
    'date_time',
    'tt_no',
    'snr_up',
    'snr_down',
    'system_code',
    'remark',
    'attend'
  ];

  ngOnInit() {

    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_e = this.route.snapshot.paramMap.get("serviceinfos");

    this.dataService.getElementView(this.subscribedParam_w).subscribe(res => {
      this.shape_id_data = res.data[0];
    });

    // get service info 
    this.progressSpinner = true;
    this.seviceinfoService.getServiceInfo(this.subscribedParam_e).subscribe(responseList => {

      if (responseList['data_found'] == false) {
        this.data_remark = responseList.data_remark;
        this.alert();
        this.progressSpinner = false;
        this.no_data = true;
      } else {
        this.progressSpinner = false;
        this.info_condition = responseList.data[0].service_condition;
        console.log(this.info_condition)
        this.cond(this.info_condition);
        this.info_equipment = responseList.data[0].equipment;
        this.info_vendor = responseList.data[0].vendor;
        this.info_curr_profile = responseList.data[0].customer_profile;
        this.info_current_profile = responseList.data[0].product_planname;
        this.info_service_id = responseList.data[0].service_id;
        this.info_prev_profile = responseList.data[0].network_profile;
        this.info_previous_profile = responseList.data[0].network_profile;
        this.info_unifi_id = responseList.data[0].unifi_id;
        this.info_pair_id = responseList.data[0].pair_id;
        this.info_from = responseList.data[0].from;
        this.info_to = responseList.data[0].to;
        this.fault_no = responseList.data[0].fault;
        this.str_last_update = responseList.data[0].last_update;
        this.Max_Attainable_Rate_up = responseList.data[0].Max_Attainable_RateUs;
        this.Max_Attainable_Rate_dw = responseList.data[0].Max_Attainable_RateDs;
        this.Attenuation_up = responseList.data[0].Attenuation_Us;
        this.Attenuation_dw = responseList.data[0].Attenuation_Ds;
        this.SNR_Margin_up = responseList.data[0].SNR_Margin_Us;
        this.SNR_Margin_dw = responseList.data[0].SNR_Margin_Ds;
        this.info_card_model = responseList.data[0].dslam_card_model;

        if (responseList.data[0].cabinet_status == "False") {
          this.cab_no = true;
        } else {
          this.cab_yes = true;
        }

        // modal table
        this.cCond = responseList.data[0].current_condition;
        this.cCond_check = this.cCond.length;
        this.cCondAffDP = responseList.data[0].current_condition;

      }

    });

    // get CTT
    this.seviceinfoService.getCTT(this.subscribedParam_e).subscribe(responseList => {

      this.cHistory = responseList.data;
      this.dataSource = new MatTableDataSource(this.cHistory);
      this.dataSourceCheck = this.cHistory.length;
      if (this.dataSourceCheck > 0) {
        this.showTable = true;
      }
      this.dataSource.paginator = this.paginator;
    });

    //graph on load - SNR MARGIN (by default)
    this.linechart = true;
    this.dailyT = false;
    this.dailyIrCr = false;
    this.pType = "1";
    this.gTitle = "SNR MARGIN";
    this.ytitle = "SNR Margin (dB)";
    this.notes = "*Min value of the day in a month";
    this.graph_snr();

  }

  // mat-table filter
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // onClickMe(event: Event) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#service-info-1',
  //         intro: help.serviceinfo[0],
  //         position: 'right'
  //       },
  //       {
  //         element: '#service-info-2',
  //         intro: help.serviceinfo[1],
  //         position: 'right'
  //       },
  //       {
  //         element: '#service-info-3',
  //         intro: help.serviceinfo[2],
  //         position: 'right'
  //       },
  //       {
  //         element: '#service-info-4',
  //         intro: help.serviceinfo[3],
  //         position: 'right'
  //       },
  //       {
  //         element: '#service-info-5',
  //         intro: help.serviceinfo[4],
  //         position: 'right'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }

  // onclick type of parameter
  paramType() {

    this.linechart = false;
    this.dailyT = true;
    this.dailyIrCr = false;

    switch (this.pType) {
      case "1":
        this.gTitle = "SNR MARGIN";
        this.ytitle = "SNR Margin (dB)";
        this.notes = "*Min value of the day in a month";
        this.graph_snr();
        break;
      case "2":
        this.gTitle = "ATTENUATION";
        this.ytitle = "Attenuationn (dB)";
        this.notes = "*Min value of the day in a month";
        this.graph_att();
        break;
      case "3":
        this.gTitle = "ATTAINABLE RATE";
        this.ytitle = "Attainable rate (Kbps)";
        this.notes = "*Min value of the day in a month";
        this.graph_attR();
        break;
      case "4":
        this.gTitle = "INSULATION RESISTANCE";
        this.ytitle = "IR (Ohm)";
        this.notes = "*Min value of the day in a month";
        this.graph_IR();
        break;
      case "5":
        this.gTitle = "CAPACITANCE RESISTANCE";
        this.ytitle = "CR (nF)";
        this.notes = "*Min value of the day in a month";
        this.graph_CR();
        break;
    }
  }

  // line chart
  lineChart(labels, uCurr, dCurr, ytitle) {

    if (this.chart) { this.chart.destroy() };

    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: labels,
        datasets: [
          {
            data: dCurr,
            borderColor: '#5A9BA1',
            borderWidth: 2,
            label: 'Downstream',
            pointStyle: 'line'
          },
          {
            data: uCurr,
            borderColor: '#EB6D3A',
            borderWidth: 2,
            label: 'Upstream',
            pointStyle: 'line'
          }
        ]
      },
      options: {
        legend: {
          display: true,
          position: 'bottom',
          fullWidth: true,
          labels: {
            usePointStyle: true
          }
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: ytitle
            }
          }],
        }
      }

    });

  };

  //line chart for IR&CR
  lineChartIRCR(labels, rab, rag, rbg, ytitle) {

    if (this.chart) { this.chart.destroy() };

    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: labels,
        datasets: [
          {
            data: rab,
            borderColor: '#5A9BA1',
            borderWidth: 2,
            label: 'RAB',
            pointStyle: 'line'
          },
          {
            data: rag,
            borderColor: '#EB6D3A',
            borderWidth: 2,
            label: 'RAG',
            pointStyle: 'line'
          },
          {
            data: rbg,
            borderColor: '#0000FE',
            borderWidth: 2,
            label: 'RBG',
            pointStyle: 'line'
          }
        ]
      },
      options: {
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle: true
          }
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: ytitle
            }
          }],
        }
      }

    });

  };

  // onclick line test
  getLineTest() {

    this.progressSpinner = true;

    this.seviceinfoService.getServiceInfoLT(this.subscribedParam_e).subscribe(responseList => {

      if (responseList['data_found'] = true) {
        this.progressSpinner = false;
      };

      this.info_condition = responseList.data[0].service_condition;
      console.log(this.info_condition)
      this.cond(this.info_condition);
      this.info_equipment = responseList.data[0].equipment;
      this.info_vendor = responseList.data[0].vendor;
      this.info_curr_profile = responseList.data[0].customer_profile;
      this.info_current_profile = responseList.data[0].product_planname;
      this.info_service_id = responseList.data[0].service_id;
      this.info_prev_profile = responseList.data[0].network_profile;
      this.info_previous_profile = responseList.data[0].network_profile;
      this.info_unifi_id = responseList.data[0].unifi_id;
      this.info_pair_id = responseList.data[0].pair_id;
      this.info_from = responseList.data[0].from;
      this.info_to = responseList.data[0].to;
      this.fault_no = responseList.data[0].fault;
      this.str_last_update = responseList.data[0].last_update;
      this.Max_Attainable_Rate_up = responseList.data[0].Max_Attainable_RateUs;
      this.Max_Attainable_Rate_dw = responseList.data[0].Max_Attainable_RateDs;
      this.Attenuation_up = responseList.data[0].Attenuation_Us;
      this.Attenuation_dw = responseList.data[0].Attenuation_Ds;
      this.SNR_Margin_up = responseList.data[0].SNR_Margin_Us;
      this.SNR_Margin_dw = responseList.data[0].SNR_Margin_Ds;
      this.info_card_model = responseList.data[0].dslam_card_model;

      // modal table
      this.cCond = responseList.data[0].current_condition;
      this.cCond_check = this.cCond.length;
      this.cCondAffDP = responseList.data[0].current_condition;

    });

  }

  // cond indicator 
  cond(cond) {

    switch (cond) {
      case "Stable":
        this.colorCond = 'text-green';
        break;
      case "Failure":
        this.colorCond = 'text-black';
        break;
      case "Caution":
        this.colorCond = 'text-yellow';
        break;
      case "Unstable":
        this.colorCond = 'text-red';
        break;
      case "N/A":
        this.colorCond = 'text-black';
        break;
      case null:
        this.colorCond = 'text-black';
        this.info_condition = "N/A";
        break;
      case "nan":
        this.colorCond = 'text-black';
        this.info_condition = "N/A";
        break;
    }

  }

  // onclick daily
  lgType_daily() {

    // this.dailyT = true;
    // this.dailyIrCr = false;
    // this.linechart = false;
    this.notes = "*Min value of the day";

    switch (this.pType) {
      case "1":
        this.dailyT = true;
        this.dailyIrCr = false;
        this.linechart = false;
        this.unit = "dB";
        this.dTitle1 = "SNR Margin Upstream";
        this.dTitle2 = "SNR Margin Downstream";
        this.dateCurr = this.sLabelsD;
        this.sUppCurr = this.sUpCurrD;
        this.sDwnCurr = this.sDwCurrD;
        break;
      case "2":
        this.dailyT = true;
        this.dailyIrCr = false;
        this.linechart = false;
        this.unit = "dB";
        this.dTitle1 = "Attenuation Upstream";
        this.dTitle2 = "Attenuation Downstream";
        this.dateCurr = this.aLabelsD;
        this.sUppCurr = this.aUpCurrD;
        this.sDwnCurr = this.aDwCurrD;
        break;
      case "3":
        this.dailyT = true;
        this.dailyIrCr = false;
        this.linechart = false;
        this.unit = "Kbps";
        this.dTitle1 = "Attainable Rate Upstream";
        this.dTitle2 = "Attainable Rate Downstream";
        this.dateCurr = this.arLabelsD;
        this.sUppCurr = this.arUpCurrD;
        this.sDwnCurr = this.arDwCurrD;
        break;
      case "4":
        this.dailyT = false;
        this.dailyIrCr = true;
        this.linechart = false;
        this.unit = "Ohm";
        this.dateCurr = this.irLabelsD;
        this.rab = this.irRAB;
        this.rag = this.irRAG;
        this.rbg = this.irRBG;
        break;
      case "5":
        this.dailyT = false;
        this.dailyIrCr = true;
        this.linechart = false;
        this.unit = "nF";
        this.dateCurr = this.crLabelsD;
        this.rab = this.crRAB;
        this.rag = this.crRAG;
        this.rbg = this.crRBG;
        break;
    }
  }

  // onclick weekly
  lgType_weekly() {

    this.linechart = true;
    this.dailyT = false;
    this.dailyIrCr = false;
    this.notes = "*Min value of the day in a week";

    switch (this.pType) {
      case "1":
        this.ytitle = "SNR Margin (dB)";
        this.lineChart(this.sLabelsW, this.sUpCurrW, this.sDwCurrW, this.ytitle);
        break;
      case "2":
        this.ytitle = "Attenuationn (dB)";
        this.lineChart(this.aLabelsW, this.aUpCurrW, this.aDwCurrW, this.ytitle);
        break;
      case "3":
        this.ytitle = "Attainable rate (Kbps)";
        this.lineChart(this.arLabelsW, this.arUpCurrW, this.arDwCurrW, this.ytitle);
        break;
      case "4":
        this.ytitle = "Insulation Resistance (Ohm)";
        this.lineChartIRCR(this.irLabelsW, this.irRABW, this.irRAGW, this.irRBGW, this.ytitle);
        break;
      case "5":
        this.ytitle = "Capacitance Resistance (nF)";
        this.lineChartIRCR(this.crLabelsW, this.crRABW, this.crRAGW, this.crRBGW, this.ytitle);
        break;
    }
  }

  // onclick monthly
  lgType_monthly() {

    this.linechart = true;
    this.dailyT = false;
    this.dailyIrCr = false;
    this.notes = "*Min value of the day in a month";

    switch (this.pType) {
      case "1":
        this.ytitle = "SNR Margin (dB)";
        this.lineChart(this.sLabelsM, this.sUpCurrM, this.sDwCurrM, this.ytitle);
        break;
      case "2":
        this.ytitle = "Attenuationn (dB)";
        this.lineChart(this.aLabelsM, this.aUpCurrM, this.aDwCurrM, this.ytitle);
        break;
      case "3":
        this.ytitle = "Attainable rate (Kbps)";
        this.lineChart(this.arLabelsM, this.arUpCurrM, this.arDwCurrM, this.ytitle);
        break;
      case "4":
        this.ytitle = "Insulation Resistance (Ohm)";
        this.lineChartIRCR(this.irLabelsM, this.irRABM, this.irRAGM, this.irRBGM, this.ytitle);
        break;
      case "5":
        this.ytitle = "Capacitance Resistance (nF)";
        this.lineChartIRCR(this.crLabelsM, this.crRABM, this.crRAGM, this.crRBGM, this.ytitle);
        break;
    }
  }

  // get latest value of SNR Margin
  graph_snr() {
    this.seviceinfoService.getGraphSNR(this.subscribedParam_e).subscribe(res => {

      this.graph_val = res.data.map(x => {
        return {
          "Daily_SNR": x.Daily_SNR,
          "Weekly_SNR": x.Weekly_SNR,
          "Monthly_SNR": x.Monthly_SNR,
        }
      });

      this.sGraphD = this.graph_val[0].Daily_SNR;
      this.sGraphW = this.graph_val[0].Weekly_SNR;
      this.sGraphM = this.graph_val[0].Monthly_SNR;

      this.sLabelsD = this.sGraphD[0].Curr_date;
      this.sUpCurrD = this.sGraphD[0].SNRUs_Curr;
      this.sDwCurrD = this.sGraphD[0].SNRDs_Curr;
      console.log(this.sLabelsD);
      console.log(this.sUpCurrD);
      console.log(this.sDwCurrD);

      this.sLabelsW = [], this.sUpCurrW = [], this.sDwCurrW = [];

      this.sGraphW.forEach(y => {
        this.sLabelsW.push(y.Weekly_Date);
        this.sUpCurrW.push(y.SNRUs_Weekly);
        this.sDwCurrW.push(y.SNRDs_Weekly);
      });

      this.sLabelsM = [], this.sUpCurrM = [], this.sDwCurrM = [];

      this.sGraphM.forEach(y => {
        this.sLabelsM.push(y.Monthly_Date);
        this.sUpCurrM.push(y.SNRUs_Monthly);
        this.sDwCurrM.push(y.SNRDs_Monthly);
      });

      // this.lineChart(this.sLabelsM, this.sUpCurrM, this.sDwCurrM, this.ytitle);
      this.lgType_daily();

    });
  }

  // get latest value of Attenuation
  graph_att() {
    this.seviceinfoService.getGraphAtt(this.subscribedParam_e).subscribe(res => {

      this.graph_val = res.data.map(x => {
        return {
          "Daily_SigAtt": x.Daily_SigAtt,
          "Weekly_SigAtt": x.Weekly_SigAtt,
          "Monthly_SigAtt": x.Monthly_SigAtt,
        }
      });

      this.cHistory = res.data[0].ctt_history;

      this.aGraphD = this.graph_val[0].Daily_SigAtt;
      this.aGraphW = this.graph_val[0].Weekly_SigAtt;
      this.aGraphM = this.graph_val[0].Monthly_SigAtt;

      this.aLabelsD = this.aGraphD[0].SigAttCurr_date;
      this.aUpCurrD = this.aGraphD[0].SigAttUs_Curr;
      this.aDwCurrD = this.aGraphD[0].SigAttDs_Curr;

      this.aLabelsW = [], this.aUpCurrW = [], this.aDwCurrW = [];
      this.aGraphW.forEach(y => {
        this.aLabelsW.push(y.Weekly_Date);
        this.aUpCurrW.push(y.sigAttUs_Weekly);
        this.aDwCurrW.push(y.sigAttDs_Weekly);
      });

      this.aLabelsM = [], this.aUpCurrM = [], this.aDwCurrM = [];
      this.aGraphM.forEach(y => {
        this.aLabelsM.push(y.Monthly_Date);
        this.aUpCurrM.push(y.SigAttUs_Monthly);
        this.aDwCurrM.push(y.SigAttDs_Monthly);
      });

      this.lgType_daily();

    });
  }

  // get latest value of Attainable Rate
  graph_attR() {
    this.seviceinfoService.getGraphAttR(this.subscribedParam_e).subscribe(res => {

      this.graph_val = res.data.map(x => {
        return {
          "Daily_MaxAttain": x.Daily_MaxAttain,
          "Weekly_MaxAttain": x.Weekly_MaxAttain,
          "Monthly_MaxAttain": x.Monthly_MaxAttain,
        }
      });

      this.arGraphD = this.graph_val[0].Daily_MaxAttain;
      this.arGraphW = this.graph_val[0].Weekly_MaxAttain;
      this.arGraphM = this.graph_val[0].Monthly_MaxAttain;

      this.arLabelsD = this.arGraphD[0].MaxAttainCurr_date;
      this.arUpCurrD = this.arGraphD[0].MaxAttainUs_Curr;
      this.arDwCurrD = this.arGraphD[0].MaxAttainDs_Curr;

      this.arLabelsW = [], this.arUpCurrW = [], this.arDwCurrW = [];
      this.arGraphW.forEach(y => {
        this.arLabelsW.push(y.Weekly_Date);
        this.arUpCurrW.push(y.MaxAttainUs_Weekly);
        this.arDwCurrW.push(y.MaxAttainDs_Weekly);
      });

      this.arLabelsM = [], this.arUpCurrM = [], this.arDwCurrM = [];
      this.arGraphM.forEach(y => {
        this.arLabelsM.push(y.Monthly_Date);
        this.arUpCurrM.push(y.MaxAttainUs_Monthly);
        this.arDwCurrM.push(y.MaxAttainDs_Monthly);
      });

      this.lgType_daily();

    });
  }

  //get latest value of IR
  graph_IR() {
    this.seviceinfoService.getGraphIR(this.subscribedParam_e).subscribe(res => {

      this.graph_val = res.data.map(x => {
        return {
          "Daily_IR": x.Daily_IR,
          "Weekly_IR": x.Weekly_IR,
          "Monthly_IR": x.Monthly_IR,
        }
      });

      this.irGraphD = this.graph_val[0].Daily_IR;
      this.irGraphW = this.graph_val[0].Weekly_IR;
      this.irGraphM = this.graph_val[0].Monthly_IR;

      this.irLabelsD = this.irGraphD[0].curr_date;
      this.irRAB = this.irGraphD[0].rab_daily;
      this.irRAG = this.irGraphD[0].rag_daily;
      this.irRBG = this.irGraphD[0].rbg_daily;

      this.irLabelsW = [], this.irRABW = [], this.irRAGW = [], this.irRBGW = [];
      this.irGraphW.forEach(y => {
        this.irLabelsW.push(y.weekly_date);
        this.irRABW.push(y.rab_weekly);
        this.irRAGW.push(y.rag_weekly);
        this.irRBGW.push(y.rbg_weekly);
      });

      this.irLabelsM = [], this.irRABM = [], this.irRAGM = [], this.irRBGM = [];
      this.irGraphM.forEach(y => {
        this.irLabelsM.push(y.monthly_date);
        this.irRABM.push(y.rab_monthly);
        this.irRAGM.push(y.rag_monthly);
        this.irRBGM.push(y.rbg_monthly);
      });

      this.lgType_daily();

    });
  }

  //get latest value of CR
  graph_CR() {
    this.seviceinfoService.getGraphCR(this.subscribedParam_e).subscribe(res => {

      this.graph_val = res.data.map(x => {
        return {
          "Daily_CR": x.Daily_CR,
          "Weekly_CR": x.Weekly_CR,
          "Monthly_CR": x.Monthly_CR,
        }
      });

      this.crGraphD = this.graph_val[0].Daily_CR;
      this.crGraphW = this.graph_val[0].Weekly_CR;
      this.crGraphM = this.graph_val[0].Monthly_CR;

      this.crLabelsD = this.crGraphD[0].curr_date;
      this.crRAB = this.crGraphD[0].cab_daily;
      this.crRAG = this.crGraphD[0].cag_daily;
      this.crRBG = this.crGraphD[0].cbg_daily;

      this.crLabelsW = [], this.crRABW = [], this.crRAGW = [], this.crRBGW = [];
      this.crGraphW.forEach(y => {
        this.crLabelsW.push(y.weekly_date);
        this.crRABW.push(y.cab_weekly);
        this.crRAGW.push(y.cag_weekly);
        this.crRBGW.push(y.cbg_weekly);
      });

      this.crLabelsM = [], this.crRABM = [], this.crRAGM = [], this.crRBGM = [];
      this.crGraphM.forEach(y => {
        this.crLabelsM.push(y.monthly_date);
        this.crRABM.push(y.cab_monthly);
        this.crRAGM.push(y.cag_monthly);
        this.crRBGM.push(y.cbg_monthly);
      });

      this.lgType_daily();

    });
  }

  alert() {
    const dialogRef = this.dialog.open(DialogAlertComponent, {
      panelClass: 'custom-dialog-container',
      width: '400px',
      data: {
        alertTitle: "Error",
        alertNotice: this.data_remark,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
