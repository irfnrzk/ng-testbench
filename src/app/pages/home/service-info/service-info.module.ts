import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginator, MatSpinner, MatPaginatorModule, MatTabsModule, MatProgressSpinnerModule, MatDialogModule, MatToolbarModule, MatTooltipModule, MatRadioModule } from '@angular/material';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { ServiceInfoComponent } from './service-info.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceInfoComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    NavpanelModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTooltipModule,
    MatRadioModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    ServiceInfoComponent
  ],
  exports: [
    ServiceInfoComponent
  ]
})
export class ServiceInfoModule { }
