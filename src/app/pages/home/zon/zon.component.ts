import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { get } from "scriptjs";

declare let simplemaps_countrymap: any;

@Component({
  selector: 'app-zon',
  templateUrl: './zon.component.html',
  styleUrls: ['./zon.component.scss']
})
export class ZonComponent implements OnInit {

  // shape data
  shape_id_data: any;
  total_zone: any;
  ArrState: any = [];
  subscribedParam: any;
  subscribedParam_2: any;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.subscribedParam = this.route.snapshot.paramMap.get("zones");
    this.subscribedParam_2 = this.route.snapshot.paramMap.get("wilayahs");

    this.dataService.getElementView(this.subscribedParam_2).subscribe(res => {
      const index = res.data[0].zone.findIndex(item => item.slug === this.subscribedParam);
      this.shape_id_data = res.data[0];
      this.total_zone = res.data[0].zone[index].node.length;
      this.ArrState = res.data[0].zone[index].node;
    });

    get("../assets/plugins/simplemaps/mapdata/mapdata_" + this.subscribedParam + ".js", () => {
      get("../assets/plugins/simplemaps/countrymap/countrymap_" + this.subscribedParam + ".js", () => {
        this.simplemapsInit();
      });
    });

  }

  simplemapsInit() {
    simplemaps_countrymap.load();
    simplemaps_countrymap.hooks.click_state = (id) => {
      const index = this.ArrState.findIndex(item => item.shape_id === id);
      this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_2 + '/zon/' + this.subscribedParam + '/node/' + this.ArrState[index].slug);
    }
  }

}


