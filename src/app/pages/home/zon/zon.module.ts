import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZonComponent } from './zon.component';
import { Routes, RouterModule } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { StatuspanelModule } from 'src/app/components/statuspanel/statuspanel.module';

const routes: Routes = [
  {
    path: '',
    component: ZonComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    StatuspanelModule,
    SidebarModule,
    NavpanelModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    ZonComponent
  ],
  exports: [
    ZonComponent
  ]

})
export class ZonModule { }
