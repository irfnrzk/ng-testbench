import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonComponent } from './zon.component';

describe('ZonComponent', () => {
  let component: ZonComponent;
  let fixture: ComponentFixture<ZonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
