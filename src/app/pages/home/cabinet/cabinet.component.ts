import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { CabinetService } from 'src/app/services/cabinet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Endpoints } from '../../../endpoint';

declare let L: any;

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.scss']
})
export class CabinetComponent implements OnInit {

  API_ENDPOINT = Endpoints.DB;
  customCollapsedHeight: string = '24px';
  customExpandedHeight: string = '24px';
  isTrue: boolean = true;
  panelOpenState: boolean = true;

  // url param
  subscribedParam_w: any;
  subscribedParam_z: any;
  subscribedParam_n: any;
  subscribedParam_c: any;

  // shape data
  shape_id_data: any;

  // leaflet
  map: any;
  geoJsonlayer: any;
  geoJsonlayer_2: any;
  geoJsonlayer_3: any;

  // exc
  checkboxExc: boolean = false;
  exch_arr: any;
  Exc_marker: any;

  // cabinet
  checkboxCabinet: boolean = true;
  cabinet_arr: any = [];
  cabinet_list: any = [];
  msan_list: any = [];
  rt_list: any = [];
  dslam_list: any = [];
  cabinet_marker: any = [];
  cabinet_data: any = [];

  // manhole
  checkboxManhole: boolean = false;
  manhole_marker: any = [];
  manhole_arr: any = [];

  // cable
  checkboxCableD: boolean = false;
  checkboxCableE: boolean = false;
  checkboxCableFibbauE: boolean = false;

  // dp
  checkboxdp: boolean = false;
  dp_marker: any = [];
  total_dp: any;
  cab_mat: any = [];

  greenIcon = L.icon({
    iconUrl: "../assets/images/dpgoodicon.png",
    iconSize: [30, 38],
    iconAnchor: [0, 0],
    popupAnchor: [15, 5]
  });

  redIcon = L.icon({
    iconUrl: "../assets/images/dpunstableicon.png",
    iconSize: [30, 38],
    iconAnchor: [0, 0],
    popupAnchor: [15, 5]
  });

  blackIcon = L.icon({
    iconUrl: "../assets/images/dpfailureicon.png",
    iconSize: [30, 38],
    iconAnchor: [0, 0],
    popupAnchor: [15, 5]
  });

  yellowIcon = L.icon({
    iconUrl: "../assets/images/dpcautionicon.png",
    iconSize: [30, 38],
    iconAnchor: [0, 0],
    popupAnchor: [15, 5]
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;
  showTable = false;
  showPaginator = true;
  showFilter = true;

  dataSource: any;
  displayedColumns: string[] = [
    'dp_list',
    'status',
    'service_length',
    // 'opt_summary',
    'action'
  ];

  constructor(
    private dataService: DataService,
    private cabinetService: CabinetService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (viewportWidth > 1919) {
      this.isTrue = false;
    }

    // fetch url param
    this.subscribedParam_n = this.route.snapshot.paramMap.get('nodes');
    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_z = this.route.snapshot.paramMap.get("zones");
    this.subscribedParam_c = this.route.snapshot.paramMap.get("cabs");

    const styleFunction = function (feature) {
      if (feature.properties.cable_clas == "D-CABLE") {
        return { color: "red" }
      } else if (feature.properties.cable_clas == "E-CABLE") {
        return { color: "green" }
      } else {
        return { color: "blue" }
      }
    };

    // fetch cable
    this.geoJsonlayer = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_copper_cable_d_all?exch_id=' + this.subscribedParam_n + "&start_ne_id=" + this.subscribedParam_c, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.geoJsonlayer_2 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_copper_cable_e?exch_id=' + this.subscribedParam_n + "&end_ne_id=" + this.subscribedParam_c, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.geoJsonlayer_3 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_fibbau_cable_e?exch_id=' + this.subscribedParam_n + "&end_ne_id=" + this.subscribedParam_c, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.dataService.getElementView(this.subscribedParam_w).subscribe(res => {

      this.shape_id_data = res.data[0]; // shape_id

      const index = this.shape_id_data.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = this.shape_id_data.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      let index_z;

      this.cabinet_list = this.shape_id_data.zone[index].node[index_w].cabinet;
      this.msan_list = this.shape_id_data.zone[index].node[index_w].msan;
      this.rt_list = this.shape_id_data.zone[index].node[index_w].rt;
      this.dslam_list = this.shape_id_data.zone[index].node[index_w].dslam;

      this.cabinet_list.forEach(element => {
        if (element.cab_id === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].cabinet.findIndex(item => item.cab_id === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].cab_id,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].long,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].status
          ];
          this.cabinet_data = this.cabinet_arr;
        }
      });

      this.msan_list.forEach(element => {
        if (element.msan_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].msan.findIndex(item => item.msan_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].msan[index_z].msan_code,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].long,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].status
          ];
          this.cabinet_data = this.cabinet_arr;
        }
      });

      this.rt_list.forEach(element => {
        if (element.rt_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].rt.findIndex(item => item.rt_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].rt[index_z].rt_code,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].long,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].status
          ];
          this.cabinet_data = this.cabinet_arr;
        }
      });

      this.dslam_list.forEach(element => {
        if (element.dslam_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].dslam.findIndex(item => item.dslam_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].dslam_code,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].long,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].status
          ];
          this.cabinet_data = this.cabinet_arr;
        }
      });

      // this.cabinet_data = this.cabinet_arr;
      console.log(this.cabinet_data);

      // map
      let grayscale = L.tileLayer.wms(
        'https://smartmap-api.tk/api/map/wms?workspace={serverWorkspace}&api_key={accessToken}',
        {
          serverWorkspace: 'Malaysia',
          accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoibW9jOG1tb0JFbzRNMDJvR1BlXzYiLCJ1c2VybmFtZSI6ImlyZmFuLnJhemFrQHRtcm5kLmNvbS5teSIsInVzZXJfdHlwZSI6InVzZXIifSwiaWF0IjoxNTU3MzY1ODg3LCJleHAiOjE1ODg5MjM0ODd9.yFUWbT911wKRPP3suohjP7PADSvWsVoxhUufrCrei9w',
          layers: 'TMSmartmap',
          format: 'image/png',
          crs: L.CRS.EPSG4326,
          tiled: true,
          attribution: 'TM SmartMap © 2019 Telekom Malaysia',
        }
      ),
        streets = L.tileLayer("http://a.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 18 });

      const init_post = [this.cabinet_arr[1], this.cabinet_arr[2]];
      // console.log(init_post);

      this.map = L.map("leaflet_map", { layers: [grayscale] }).setView(init_post, 18);

      var baseMaps = {
        "TM Smart Map": grayscale,
        "OpenStreetMap": streets
      };

      L.control.layers(baseMaps, null, { position: 'bottomright' }).addTo(this.map);
      L.control.scale().addTo(this.map);

      // // inv
      this.exch_arr = [
        this.shape_id_data.zone[index].node[index_w].shape_id,
        this.shape_id_data.zone[index].node[index_w].lat,
        this.shape_id_data.zone[index].node[index_w].long,
        this.shape_id_data.zone[index].node[index_w].status,
      ]

      // this.manhole_arr = res.body.data[3].data.map(e => { return [e.manhole_id, e.y, e.x]; });
      this.checkCabinet('A');

    });

    this.cabinetService.getDPbyCabinet(this.subscribedParam_c).subscribe(res => {
      // console.log(res.data);
      this.cab_mat = res.data;
      this.total_dp = this.cab_mat.length;
      this.dataSource = new MatTableDataSource(this.cab_mat);
      this.dataSource.paginator = this.paginator;
      // console.log(this.dataSource);
      if (this.dataSource.data.length > 0) {
        this.showTable = true;
        this.showPaginator = false;
        this.showFilter = false;
      } else {
        this.showTable = false;
        this.showPaginator = true;
        this.showFilter = true;
      }
      // console.log(this.showTable);
      // this.progressSpinner = false;

    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  showExc() {

    let redIcon;
    if (this.exch_arr[3] == 'Good') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodegoodicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Caution') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodecautionicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Unstable') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodeunstableicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodefailureicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    }

    this.Exc_marker = L.marker([this.exch_arr[1], this.exch_arr[2]], {
      icon: redIcon
    }).bindPopup(`${this.exch_arr[0]}<br><a href="http://www.google.com/maps/place/${this.exch_arr[1]},${this.exch_arr[2]}" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get Direction</a>`).addTo(this.map).openPopup();
  }

  checkExc(event: any) {
    if (event == 'A') {
      this.showExc();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 18);
    } else { this.map.removeLayer(this.Exc_marker); }
  }

  showCabinet() {
    let redIcon: any;
    if (this.cabinet_arr[3] == 'Good') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetgoodicon.png",
        iconSize: [30, 38],
        iconAnchor: [0, 0],
        popupAnchor: [15, 5]
      });
    } else if (this.cabinet_arr[3] == 'Caution') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetcautionicon.png",
        iconSize: [30, 38],
        iconAnchor: [0, 0],
        popupAnchor: [15, 5]
      });
    } else if (this.cabinet_arr[3] == 'Unstable') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetunstableicon.png",
        iconSize: [30, 38],
        iconAnchor: [0, 0],
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetfailureicon.png",
        iconSize: [30, 38],
        iconAnchor: [0, 0],
        popupAnchor: [15, 5]
      });
    }

    let html = `${this.cabinet_arr[0]}<br><a href="http://www.google.com/maps/place/${this.cabinet_arr[1]},${this.cabinet_arr[2]}" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get Direction</a>`;

    this.cabinet_marker = L.marker([this.cabinet_arr[1], this.cabinet_arr[2]], {
      icon: redIcon
    }).bindPopup(html).addTo(this.map).openPopup();
  }

  checkCabinet(event: any) {
    if (event == 'A') {
      this.showCabinet();
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 18);
    } else {
      this.map.removeLayer(this.cabinet_marker);
    }
  }

  checkCableD(event: any) {
    if (event == 'A') {
      this.geoJsonlayer.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer); }
  }

  checkCableE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_2.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_2); }
  }

  checkCableFibbauE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_3.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_3); }
  }

  showManhole() {
    var redIcon = L.icon({
      iconUrl: "../assets/images/manhole_icon.png",
      iconSize: [30, 30],
      iconAnchor: [0, 0],
      popupAnchor: [12, 0]
    });

    for (let i = 0; i < this.manhole_arr.length; i++) {
      let html = `${this.manhole_arr[i][0]}`;
      this.manhole_marker[i] = L.marker([this.manhole_arr[i][1], this.manhole_arr[i][2]], {
        icon: redIcon
      }).bindPopup(html).addTo(this.map);
    }
  }

  checkManhole(event: any) {
    if (event == 'A') {
      this.showManhole();
    } else {
      for (let i = 0; i < this.manhole_arr.length; i++) {
        this.map.removeLayer(this.manhole_marker[i]);
      }
    }
  }

  checkdp(event: any) {
    if (event == 'A') {
      this.showdp();
      this.map.flyTo([this.cab_mat[0].lat, this.cab_mat[0].long], 15);
    } else {
      for (let i = 0; i < this.cab_mat.length; i++) {
        this.map.removeLayer(this.dp_marker[i]);
      }
    }
  }

  showdp() {
    for (let i = 0; i < this.cab_mat.length; i++) {
      let html = `${this.cab_mat[i].dp_id}`;

      if (this.cab_mat[i].status == "1") {
        this.dp_marker[i] = L.marker([this.cab_mat[i].lat, this.cab_mat[i].long], {
          icon: this.greenIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.cab_mat[i].status == "2") {
        this.dp_marker[i] = L.marker([this.cab_mat[i].lat, this.cab_mat[i].long], {
          icon: this.yellowIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.cab_mat[i].status == "3") {
        this.dp_marker[i] = L.marker([this.cab_mat[i].lat, this.cab_mat[i].long], {
          icon: this.redIcon
        }).bindPopup(html).addTo(this.map);
      } else {
        this.dp_marker[i] = L.marker([this.cab_mat[i].lat, this.cab_mat[i].long], {
          icon: this.blackIcon
        }).bindPopup(html).addTo(this.map);
      }
    }
  }

  // navigate to DP view
  redirectHandler(path: any): void {
    console.log(path);
    const index = this.cab_mat.findIndex(item => item.dp_id === path);
    if (this.cab_mat[index].long == "") {
      alert(this.cab_mat[index].data_remark);
    }
    // else {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/dp/' + path);
    // }
  }

  redirectHandler2(path: any): void {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/netinfo/' + path);
  }

}
