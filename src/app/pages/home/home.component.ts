import { Component, OnInit } from '@angular/core';
import { get } from "scriptjs";
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

declare var simplemaps_countrymap: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  ArrState: any = [];
  state: string;

  constructor(
    private router: Router,
    private dataservice: DataService
  ) { }

  ngOnInit() {
    this.dataservice.getOverallView()
      .subscribe(res => {
        this.ArrState = res.data;
        this.state = this.ArrState[0];
      });

    get("../assets/plugins/simplemaps/mapdata/mapdata_petamalaysia.js", () => {
      get("../assets/plugins/simplemaps/countrymap/countrymap_petamalaysia.js", () => {
        this.simplemapsInit();
      });
    });
  }

  simplemapsInit() {
    simplemaps_countrymap.load();
    simplemaps_countrymap.hooks.click_state = ((id: string) => {
      const index = this.ArrState.findIndex((item: any) => item.state_id === id);
      this.router.navigateByUrl('copper/wilayah/' + this.ArrState[index].slug);
    });
  }

}
