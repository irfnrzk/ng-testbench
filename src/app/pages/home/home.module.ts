import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { StatuspanelModule } from 'src/app/components/statuspanel/statuspanel.module';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    StatuspanelModule,
    SidebarModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule { }