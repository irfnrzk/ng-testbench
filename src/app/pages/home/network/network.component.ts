import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CabinetService } from 'src/app/services/cabinet.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Endpoints } from '../../../endpoint';

declare let L;

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {

  API_ENDPOINT = Endpoints.DB;
  customCollapsedHeight: string = '24px';
  customExpandedHeight: string = '24px';
  isTrue: boolean = true;
  panelOpenState: boolean = true;

  // url param
  subscribedParam_w: any;
  subscribedParam_z: any;
  subscribedParam_n: any;
  subscribedParam_c: any;
  subscribedParam_d: any;
  subscribedParam_f: any;

  // shape data
  shape_id_data: any;

  // leaflet
  map: any;
  geoJsonlayer: any;
  geoJsonlayer_2: any;
  geoJsonlayer_3: any;
  geoJsonfault: any;

  // exc
  checkboxExc: boolean = false;
  exch_arr: any;
  Exc_marker: any;

  // cabinet
  checkboxCabinet: boolean = false;
  cabinet_marker: any = [];
  cabinet_arr: any = [];
  cabinet_list: any = [];
  msan_list: any = [];
  rt_list: any = [];
  dslam_list: any = [];

  // manhole
  checkboxManhole: boolean = false;
  checkManhole: any;
  manhole_marker: any = [];
  manhole_arr: any = [];

  // cable
  checkboxCableD: boolean = false;
  checkboxCableE: boolean = false;
  checkboxCableFibbauE: boolean = false;

  // dp
  checkboxdp: boolean = true;
  dp_arr: any = [];
  dp_marker: any = [];
  dp_lists: any = [];
  total_dp: any;

  // pair
  total_service: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  showTable = false;

  dataSource: any;
  displayedColumns: string[] = [
    'dp_pair_id',
    'status',
    'service_length',
    // 'opt_summary',
    // 'prediction',
    'action'
  ];

  constructor(
    private dataService: DataService,
    private cabinetService: CabinetService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (viewportWidth > 1919) {
      this.isTrue = false;
    }

    // fetch url param
    this.subscribedParam_n = this.route.snapshot.paramMap.get('nodes');
    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_z = this.route.snapshot.paramMap.get("zones");
    this.subscribedParam_c = this.route.snapshot.paramMap.get("cabs");
    this.subscribedParam_d = this.route.snapshot.paramMap.get("dps");
    this.subscribedParam_f = this.route.snapshot.paramMap.get("faults");

    const styleFunction = function (feature) {
      if (feature.properties.cable_clas == "D-CABLE") {
        return { color: "red" }
      } else if (feature.properties.cable_clas == "E-CABLE") {
        return { color: "green" }
      } else {
        return { color: "blue" }
      }
    };

    // fetch cable
    this.geoJsonlayer = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_copper_cable_d?exch_id=' + this.subscribedParam_n + "&end_ne_id=" + this.subscribedParam_d, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.geoJsonlayer_2 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_copper_cable_e?exch_id=' + this.subscribedParam_n + "&end_ne_id=" + this.subscribedParam_c, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.geoJsonlayer_3 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_fibbau_cable_e?exch_id=' + this.subscribedParam_n + "&end_ne_id=" + this.subscribedParam_c, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.dataService.getElementView(this.subscribedParam_w).subscribe(res => {

      this.shape_id_data = res.data[0]; // shape_id      
      const index = this.shape_id_data.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = this.shape_id_data.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);
      // const index_z = this.shape_id_data.zone[index].node[index_w].cabinet.findIndex(item => item.cab_id === this.subscribedParam_c);

      var index_z;

      this.cabinet_list = this.shape_id_data.zone[index].node[index_w].cabinet;
      this.msan_list = this.shape_id_data.zone[index].node[index_w].msan;
      this.rt_list = this.shape_id_data.zone[index].node[index_w].rt;
      this.dslam_list = this.shape_id_data.zone[index].node[index_w].dslam;

      this.cabinet_list.forEach(element => {
        if (element.cab_id === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].cabinet.findIndex(item => item.cab_id === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].cab_id,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].long,
            this.shape_id_data.zone[index].node[index_w].cabinet[index_z].status
          ];
        }
      });

      this.msan_list.forEach(element => {
        if (element.msan_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].msan.findIndex(item => item.msan_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].msan[index_z].msan_code,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].long,
            this.shape_id_data.zone[index].node[index_w].msan[index_z].status
          ];
        }
      });

      this.rt_list.forEach(element => {
        if (element.rt_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].rt.findIndex(item => item.rt_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].rt[index_z].rt_code,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].long,
            this.shape_id_data.zone[index].node[index_w].rt[index_z].status
          ];
        }
      });

      this.dslam_list.forEach(element => {
        if (element.dslam_code === this.subscribedParam_c) {
          index_z = this.shape_id_data.zone[index].node[index_w].dslam.findIndex(item => item.dslam_code === this.subscribedParam_c);
          this.cabinet_arr = [
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].dslam_code,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].lat,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].long,
            this.shape_id_data.zone[index].node[index_w].dslam[index_z].status
          ];
        }
      });

      // // inv
      this.exch_arr = [
        this.shape_id_data.zone[index].node[index_w].shape_id,
        this.shape_id_data.zone[index].node[index_w].lat,
        this.shape_id_data.zone[index].node[index_w].long,
        this.shape_id_data.zone[index].node[index_w].status
      ]
      // this.manhole_arr = res.body.data[3].data.map(e => { return [e.manhole_id, e.y, e.x]; });

    });

    this.cabinetService.getDPbyCabinet(this.subscribedParam_c).subscribe(res => {
      const index = res.data.findIndex(item => item.dp_id === this.subscribedParam_d);
      this.dp_arr = res.data[index];

      // map
      let grayscale = L.tileLayer.wms(
        'https://smartmap-api.tk/api/map/wms?workspace={serverWorkspace}&api_key={accessToken}',
        {
          serverWorkspace: 'Malaysia',
          accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoibW9jOG1tb0JFbzRNMDJvR1BlXzYiLCJ1c2VybmFtZSI6ImlyZmFuLnJhemFrQHRtcm5kLmNvbS5teSIsInVzZXJfdHlwZSI6InVzZXIifSwiaWF0IjoxNTU3MzY1ODg3LCJleHAiOjE1ODg5MjM0ODd9.yFUWbT911wKRPP3suohjP7PADSvWsVoxhUufrCrei9w',
          layers: 'TMSmartmap',
          format: 'image/png',
          crs: L.CRS.EPSG4326,
          tiled: true,
          attribution: 'TM SmartMap © 2019 Telekom Malaysia',
        }
      ),
        streets = L.tileLayer("http://a.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 18 });

      const init_post = [this.dp_arr.lat, this.dp_arr.long];
      this.map = L.map("leaflet_map", { layers: [grayscale] }).setView(init_post, 18);

      var baseMaps = {
        "TM Smart Map": grayscale,
        "OpenStreetMap": streets
      };

      L.control.layers(baseMaps, null, { position: 'bottomright' }).addTo(this.map);
      L.control.scale().addTo(this.map);

      this.checkdp('A');
      if (this.subscribedParam_f) {
        this.addFault();
      }
    });

    // DP Pair list mat-table
    this.dataService.getDPPairbyDP(this.subscribedParam_d).subscribe(res => {
      res.data.forEach(el => {
        if (el.dp_pair_id !== null) {
          this.total_service.push({
            "dp_pair_id": el.dp_pair_id
          });
        }
      });

      this.dataSource = new MatTableDataSource(this.total_service);
      this.dataSource.paginator = this.paginator;
      if (this.dataSource.data.length > 0) {
        this.showTable = true;
      }
    })

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addFault() {
    const styleFunction = function (feature) {
      if (feature.properties.cable_clas == "D-CABLE") {
        return { color: "black", opacity: "0.5" }
      } else if (feature.properties.cable_clas == "E-CABLE") {
        return { color: "green" }
      } else {
        return { color: "blue" }
      }
    };
    var fault = L.icon({
      iconUrl: "../assets/images/marker-icon.png",
      iconSize: [25, 41],
      iconAnchor: [12.5, 41],
      popupAnchor: [0, -41]
    });

    // fetch cable
    this.geoJsonfault = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/get_fault_cable_dside?exch_id=' + this.subscribedParam_n + '&end_ne_id=' + this.subscribedParam_d + '&fault_dist=' + this.subscribedParam_f, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
          // layer.bindPopup("Fault : " + feature.properties.fault_in_metres);
        }
      },
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, {
          icon: fault
        })
      }
    }).addTo(this.map);
  }

  checkdp(event: any) {
    if (event == 'A') {
      this.showdp();
      this.map.flyTo([this.dp_arr.lat, this.dp_arr.long], 18);
    } else {
      this.map.removeLayer(this.dp_marker);
    }
  }

  showdp() {
    let redIcon;
    if (this.dp_arr.status == '1') {
      redIcon = L.icon({
        iconUrl: "../assets/images/dpgoodicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.dp_arr.status == '2') {
      redIcon = L.icon({
        iconUrl: "../assets/images/dpcautionicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.dp_arr.status == '3') {
      redIcon = L.icon({
        iconUrl: "../assets/images/dpunstableicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/dpfailureicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    }

    this.dp_marker = L.marker([this.dp_arr.lat, this.dp_arr.long], {
      icon: redIcon
    })
      .bindPopup(`${this.dp_arr.dp_id}<br><a href="http://www.google.com/maps/place/${this.dp_arr.lat},${this.dp_arr.long}" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get Direction</a>`)
      .addTo(this.map).openPopup();
  }

  showCabinet() {
    let redIcon;
    if (this.cabinet_arr[3] == 'Good') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetgoodicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.cabinet_arr[3] == 'Caution') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetcautionicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.cabinet_arr[3] == 'Unstable') {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetunstableicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/cabinetfailureicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    }

    let html = this.cabinet_arr[0];
    this.cabinet_marker = L.marker([this.cabinet_arr[1], this.cabinet_arr[2]], {
      icon: redIcon
    }).bindPopup(html).addTo(this.map).openPopup();;
  }

  checkCabinet(event: any) {
    if (event == 'A') {
      this.showCabinet();
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 18);
    } else {
      this.map.removeLayer(this.cabinet_marker);
    }
  }

  checkCableD(event: any) {
    if (event == 'A') {
      this.geoJsonlayer.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer); }
  }

  checkCableE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_2.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_2); }
  }
  checkCableFibbauE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_3.addTo(this.map);
      this.map.flyTo([this.cabinet_arr[1], this.cabinet_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_3); }
  }

  showExc() {
    let redIcon;
    if (this.exch_arr[3] == 'Good') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodegoodicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Caution') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodecautionicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Unstable') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodeunstableicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodefailureicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    }

    this.Exc_marker = L.marker([this.exch_arr[1], this.exch_arr[2]], {
      icon: redIcon
    }).bindPopup(`${this.exch_arr[0]}<br><a href="http://www.google.com/maps/place/${this.exch_arr[1]},${this.exch_arr[2]}" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get Direction</a>`).addTo(this.map).openPopup();
  }

  checkExc(event: any) {
    if (event == 'A') {
      this.showExc();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 18);
    } else { this.map.removeLayer(this.Exc_marker); }
  }

  // navigate to service info view
  redirectHandler(path: any): void {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/dp/' + this.subscribedParam_d + '/serviceinfo/' + path);
    // console.log('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/dp/' + this.subscribedParam_d + '/serviceinfo/' + path);
  }

}
