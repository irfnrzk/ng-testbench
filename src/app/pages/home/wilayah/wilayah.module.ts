import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { WilayahComponent } from './wilayah.component';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { StatuspanelModule } from 'src/app/components/statuspanel/statuspanel.module';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';

const routes: Routes = [
  {
    path: '',
    component: WilayahComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    StatuspanelModule,
    SidebarModule,
    NavpanelModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    WilayahComponent
  ]
})
export class WilayahModule { }
