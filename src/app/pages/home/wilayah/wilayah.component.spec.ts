import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WilayahComponent } from './wilayah.component';

describe('WilayahComponent', () => {
  let component: WilayahComponent;
  let fixture: ComponentFixture<WilayahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WilayahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WilayahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
