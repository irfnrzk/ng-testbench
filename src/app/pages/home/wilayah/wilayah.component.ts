import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { get } from "scriptjs";

declare let simplemaps_countrymap: any;

@Component({
  selector: 'app-wilayah',
  templateUrl: './wilayah.component.html',
  styleUrls: ['./wilayah.component.scss']
})
export class WilayahComponent implements OnInit {

  ArrState: any = [];
  subscribedParam: any;
  shape_id_data: any;
  total_zone: number = 0;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.subscribedParam = this.route.snapshot.paramMap.get("wilayahs");

    this.dataService.getElementView(this.subscribedParam).subscribe(res => {
      console.log(res.data);
      this.shape_id_data = res.data[0]; // shape_id
      this.total_zone = res.data[0].zone.length;
      this.ArrState = res.data[0].zone;
    });

    get("../assets/plugins/simplemaps/mapdata/mapdata_" + this.subscribedParam + ".js", () => {
      get("../assets/plugins/simplemaps/countrymap/countrymap_" + this.subscribedParam + ".js", () => {
        this.simplemapsInit();
      });
    });

  }

  simplemapsInit() {
    simplemaps_countrymap.load();
    simplemaps_countrymap.hooks.click_state = (id) => {
      const index = this.ArrState.findIndex(item => item.shape_id === id);
      this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam + '/zon/' + this.ArrState[index].slug);
    }
  }

}
