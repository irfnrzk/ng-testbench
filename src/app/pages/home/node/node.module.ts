import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NeedAuthGuard } from 'src/app/auth.guard';
import { StatuspanelModule } from 'src/app/components/statuspanel/statuspanel.module';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { NodeComponent } from './node.component';
import { NavpanelModule } from 'src/app/components/navpanel/navpanel.module';
import { MatFormFieldModule, MatTableModule, MatPaginatorModule, MatInputModule, MatExpansionModule } from '@angular/material';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: NodeComponent,
    canActivate: [NeedAuthGuard]
  },
];

@NgModule({
  imports: [
    CommonModule,
    // BrowserAnimationsModule,
    StatuspanelModule,
    SidebarModule,
    NavpanelModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    NodeComponent
  ],
  exports: [
    NodeComponent
  ]

})
export class NodeModule { }
