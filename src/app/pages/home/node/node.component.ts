import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Endpoints } from '../../../endpoint';
import { NodeService } from 'src/app/services/node.service';

declare let L: any;

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {

  API_ENDPOINT = Endpoints.DB;
  customCollapsedHeight: string = '24px';
  customExpandedHeight: string = '24px';
  isTrue: boolean = true;
  panelOpenState: boolean = true;

  // url param
  subscribedParam_w: string;
  subscribedParam_z: string;
  subscribedParam_n: string;

  // shape data
  shape_id_data: any;

  // leaflet
  map: any;
  geoJsonlayer: any;
  geoJsonlayer_2: any;
  geoJsonlayer_3: any;

  // boundary
  checkboxboundary: boolean = false;
  boundaryjson: any;

  // exc
  checkboxExc: boolean = true;
  exch_arr: any;
  Exc_marker: any;

  // cabinet
  total_cabinet: any;
  checkboxCabinet: boolean = false;
  checkboxMsan: boolean = false;
  checkboxDslam: boolean = false;
  checkboxRt: boolean = false;
  cabinet_arr: any = [];
  cabinet_list: any = [];
  msan_list: any = [];
  rt_list: any = [];
  dslam_list: any = [];
  selectedcab: any = [];
  selectedcabinet: any = [];
  totalarray: any = [];

  // manhole
  checkboxManhole: boolean = false;
  manhole_marker: any = [];
  manhole_arr: any = [];

  // cable
  checkboxCableD: boolean = false;
  checkboxCableE: boolean = false;
  checkboxCableFibbauE: boolean = false;

  // marker icon
  cabinet_marker: any = [];
  msan_marker: any = [];
  dslam_marker: any = [];
  rt_marker: any = [];

  // checkboxes

  greenIcon = L.icon({
    iconUrl: "../assets/images/cabinetgoodicon.png",
    iconSize: [30, 38], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [15, 5]
  });

  redIcon = L.icon({
    iconUrl: "../assets/images/cabinetunstableicon.png",
    iconSize: [30, 38], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [15, 5]
  });

  blackIcon = L.icon({
    iconUrl: "../assets/images/cabinetfailureicon.png",
    iconSize: [30, 38], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [15, 5]
  });

  yellowIcon = L.icon({
    iconUrl: "../assets/images/cabinetcautionicon.png",
    iconSize: [30, 38], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [15, 5]
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: any;
  displayedColumns: string[] = [
    'cabinet_list',
    'cabinet_type',
    'status',
    'service_length',
    // 'opt_summary',
    // 'prediction',
    'action'
  ];

  constructor(
    private dataService: DataService,
    private nodeService: NodeService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (viewportWidth > 1919) {
      this.isTrue = false;
    }

    // fetch url param
    this.subscribedParam_n = this.route.snapshot.paramMap.get('nodes');
    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_z = this.route.snapshot.paramMap.get("zones");

    const styleFunction = function (feature) {
      if (feature.properties.cable_clas == "D-CABLE") {
        return { color: "red" }
      } else if (feature.properties.cable_clas == "E-CABLE") {
        return { color: "green" }
      } else {
        return { color: "blue" }
      }
    };

    // fetch boundary
    this.nodeService.NodeApi(this.subscribedParam_n).subscribe(res => {
      // console.log(res);
      this.boundaryjson = L.geoJSON(res[3], {
        style: styleFunction,
        weight: 3,
        onEachFeature: function (feature, layer) {
          if (feature.properties) {
            layer.bindPopup(Object.keys(feature.properties).map(function (k) {
              return k + ": " + feature.properties[k];
            }).join("<br />"), {
                maxHeight: 200
              });
          }
        }
      });
    });

    // fetch cable
    // console.log(this.geoJsonlayer);
    this.geoJsonlayer = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/get_copper_cable?exch_id=' + this.subscribedParam_n + "&cable_class=D", {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });
    // console.log(this.geoJsonlayer);
    // setTimeout(() => {
    //   console.log(this.geoJsonlayer.layers);
    // }, 15000);

    this.geoJsonlayer_2 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/get_copper_cable?exch_id=' + this.subscribedParam_n + "&cable_class=E", {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.geoJsonlayer_3 = L.geoJSON.ajax(this.API_ENDPOINT + 'dylia/api/inventory/get_fibbau_cable_e_all?exch_id=' + this.subscribedParam_n, {
      style: styleFunction,
      weight: 3,
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          layer.bindPopup(Object.keys(feature.properties).map(function (k) {
            return k + ": " + feature.properties[k];
          }).join("<br />"), {
              maxHeight: 200
            });
        }
      }
    });

    this.dataService.getElementView(this.subscribedParam_w).subscribe(res => {

      this.shape_id_data = res.data[0]; // shape_id
      const index = this.shape_id_data.zone.findIndex(item => item.shape_id === this.subscribedParam_z);
      const index_w = this.shape_id_data.zone[index].node.findIndex(item => item.shape_id === this.subscribedParam_n);

      // Cabinet all-type list
      this.cabinet_list = this.shape_id_data.zone[index].node[index_w].cabinet;
      this.cabinet_list.forEach(element => {
        element.cab_type = "CABINET";
        this.totalarray.push(element);
      });

      this.msan_list = this.shape_id_data.zone[index].node[index_w].msan;
      this.msan_list.forEach(element => {
        element.cab_id = element.msan_code;
        element.cab_type = "MSAN";
        this.totalarray.push(element);
      });

      this.rt_list = this.shape_id_data.zone[index].node[index_w].rt;
      this.rt_list.forEach(element => {
        element.cab_id = element.rt_code;
        element.cab_type = "RT";
        this.totalarray.push(element);
      });

      this.dslam_list = this.shape_id_data.zone[index].node[index_w].dslam;
      this.dslam_list.forEach(element => {
        element.cab_id = element.dslam_code;
        element.cab_type = "DSLAM";
        this.totalarray.push(element);
      });

      // console.log(this.totalarray);
      this.total_cabinet = this.totalarray.length;
      this.dataSource = new MatTableDataSource(this.totalarray);
      this.dataSource.paginator = this.paginator;

      let grayscale = L.tileLayer.wms(
        'https://smartmap-api.tk/api/map/wms?workspace={serverWorkspace}&api_key={accessToken}',
        {
          serverWorkspace: 'Malaysia',
          accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoibW9jOG1tb0JFbzRNMDJvR1BlXzYiLCJ1c2VybmFtZSI6ImlyZmFuLnJhemFrQHRtcm5kLmNvbS5teSIsInVzZXJfdHlwZSI6InVzZXIifSwiaWF0IjoxNTU3MzY1ODg3LCJleHAiOjE1ODg5MjM0ODd9.yFUWbT911wKRPP3suohjP7PADSvWsVoxhUufrCrei9w',
          layers: 'TMSmartmap',
          format: 'image/png',
          crs: L.CRS.EPSG4326,
          tiled: true,
          attribution: 'TM SmartMap © 2019 Telekom Malaysia',
        }
      ),
        streets = L.tileLayer("http://a.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 18 });

      // map
      this.map = L.map("leaflet_map", {
        layers: [grayscale]
      }).setView([this.shape_id_data.zone[index].node[index_w].lat, this.shape_id_data.zone[index].node[index_w].long], 12);

      var baseMaps = {
        "TM Smart Map": grayscale,
        "OpenStreetMap": streets
      };

      L.control.layers(baseMaps, null, { position: 'bottomright' }).addTo(this.map);
      L.control.scale().addTo(this.map);

      // inv
      this.exch_arr = [
        this.shape_id_data.zone[index].node[index_w].shape_id,
        this.shape_id_data.zone[index].node[index_w].lat,
        this.shape_id_data.zone[index].node[index_w].long,
        this.shape_id_data.zone[index].node[index_w].status,
      ]
      this.cabinet_arr = this.cabinet_list.map(e => { return [e.cab_id, e.lat, e.long, e.status]; });
      this.checkExc('A');

    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  checkCableD(event: any) {
    if (event == 'A') {
      this.geoJsonlayer.addTo(this.map);
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer); }
  }

  checkCableE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_2.addTo(this.map);
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_2); }
  }

  checkCableFibbauE(event: any) {
    if (event == 'A') {
      this.geoJsonlayer_3.addTo(this.map);
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else { this.map.removeLayer(this.geoJsonlayer_3); }
  }

  showExc() {

    let redIcon;
    if (this.exch_arr[3] == 'Good') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodegoodicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Caution') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodecautionicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else if (this.exch_arr[3] == 'Unstable') {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodeunstableicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    } else {
      redIcon = L.icon({
        iconUrl: "../assets/images/nodefailureicon.png",
        iconSize: [30, 38], // size of the icon
        iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
        popupAnchor: [15, 5]
      });
    }

    this.Exc_marker = L.marker([this.exch_arr[1], this.exch_arr[2]], {
      icon: redIcon
    }).bindPopup(`${this.exch_arr[0]}<br><a href="http://www.google.com/maps/place/${this.exch_arr[1]},${this.exch_arr[2]}" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get Direction</a>`).addTo(this.map).openPopup();
  }

  checkExc(event: any) {
    if (event == 'A') {
      this.showExc();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 18);
    } else { this.map.removeLayer(this.Exc_marker); }
  }

  checkboundary(event: any) {
    if (event == 'A') {
      this.boundaryjson.addTo(this.map);
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 11);
    } else { this.map.removeLayer(this.boundaryjson); }
  }

  showCabinet() {
    for (let i = 0; i < this.cabinet_arr.length; i++) {
      let html = `${this.cabinet_arr[i][0]}`;

      if (this.cabinet_arr[i][3] == 'Good') {
        this.cabinet_marker[i] = L.marker([this.cabinet_arr[i][1], this.cabinet_arr[i][2]], {
          icon: this.greenIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.cabinet_arr[i][3] == 'Caution') {
        this.cabinet_marker[i] = L.marker([this.cabinet_arr[i][1], this.cabinet_arr[i][2]], {
          icon: this.yellowIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.cabinet_arr[i][3] == 'Unstable') {
        this.cabinet_marker[i] = L.marker([this.cabinet_arr[i][1], this.cabinet_arr[i][2]], {
          icon: this.redIcon
        }).bindPopup(html).addTo(this.map);
      } else {
        this.cabinet_marker[i] = L.marker([this.cabinet_arr[i][1], this.cabinet_arr[i][2]], {
          icon: this.blackIcon
        }).bindPopup(html).addTo(this.map);
      }
    }
  }

  checkCabinet(event: any) {
    if (event == 'A') {
      this.showCabinet();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else {
      for (let i = 0; i < this.cabinet_arr.length; i++) {
        this.map.removeLayer(this.cabinet_marker[i]);
      }
    }
  }

  showMsan() {
    for (let i = 0; i < this.msan_list.length; i++) {
      let html = `${this.msan_list[i].msan_code}`;

      if (this.msan_list[i].status == 'Good') {
        this.msan_marker[i] = L.marker([this.msan_list[i].lat, this.msan_list[i].long], {
          icon: this.greenIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.msan_list[i].status == 'Caution') {
        this.msan_marker[i] = L.marker([this.msan_list[i].lat, this.msan_list[i].long], {
          icon: this.yellowIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.msan_list[i].status == 'Unstable') {
        this.msan_marker[i] = L.marker([this.msan_list[i].lat, this.msan_list[i].long], {
          icon: this.redIcon
        }).bindPopup(html).addTo(this.map);
      } else {
        this.msan_marker[i] = L.marker([this.msan_list[i].lat, this.msan_list[i].long], {
          icon: this.blackIcon
        }).bindPopup(html).addTo(this.map);
      }
    }
  }

  checkMsan(event: any) {
    if (event == 'A') {
      this.showMsan();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else {
      for (let i = 0; i < this.msan_list.length; i++) {
        this.map.removeLayer(this.msan_marker[i]);
      }
    }
  }

  showDslam() {
    for (let i = 0; i < this.dslam_list.length; i++) {
      let html = `${this.dslam_list[i].dslam_code}`;

      if (this.dslam_list[i].status == 'Good') {
        this.dslam_marker[i] = L.marker([this.dslam_list[i].lat, this.dslam_list[i].long], {
          icon: this.greenIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.dslam_list[i].status == 'Caution') {
        this.dslam_marker[i] = L.marker([this.dslam_list[i].lat, this.dslam_list[i].long], {
          icon: this.yellowIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.dslam_list[i].status == 'Unstable') {
        this.dslam_marker[i] = L.marker([this.dslam_list[i].lat, this.dslam_list[i].long], {
          icon: this.redIcon
        }).bindPopup(html).addTo(this.map);
      } else {
        this.dslam_marker[i] = L.marker([this.dslam_list[i].lat, this.dslam_list[i].long], {
          icon: this.blackIcon
        }).bindPopup(html).addTo(this.map);
      }
    }
  }

  checkDslam(event: any) {
    if (event == 'A') {
      this.showDslam();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else {
      for (let i = 0; i < this.dslam_list.length; i++) {
        this.map.removeLayer(this.dslam_marker[i]);
      }
    }
  }

  showRt() {
    for (let i = 0; i < this.rt_list.length; i++) {
      let html = `${this.rt_list[i].rt_code}`;

      if (this.rt_list[i].status == 'Good') {
        this.rt_marker[i] = L.marker([this.rt_list[i].lat, this.rt_list[i].long], {
          icon: this.greenIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.rt_list[i].status == 'Caution') {
        this.rt_marker[i] = L.marker([this.rt_list[i].lat, this.rt_list[i].long], {
          icon: this.yellowIcon
        }).bindPopup(html).addTo(this.map);
      } else if (this.rt_list[i].status == 'Unstable') {
        this.rt_marker[i] = L.marker([this.rt_list[i].lat, this.rt_list[i].long], {
          icon: this.redIcon
        }).bindPopup(html).addTo(this.map);
      } else {
        this.rt_marker[i] = L.marker([this.rt_list[i].lat, this.rt_list[i].long], {
          icon: this.blackIcon
        }).bindPopup(html).addTo(this.map);
      }
    }
  }

  checkRt(event: any) {
    if (event == 'A') {
      this.showRt();
      this.map.flyTo([this.exch_arr[1], this.exch_arr[2]], 13);
    } else {
      for (let i = 0; i < this.rt_list.length; i++) {
        this.map.removeLayer(this.rt_marker[i]);
      }
    }
  }

  showManhole() {
    var redIcon = L.icon({
      iconUrl: "../assets/images/manhole_icon.png",
      iconSize: [30, 30], // size of the icon
      iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
      popupAnchor: [12, 0]
    });
    for (let i = 0; i < this.manhole_arr.length; i++) {
      let html = `${this.manhole_arr[i][0]}`;
      this.manhole_marker[i] = L.marker([this.manhole_arr[i][1], this.manhole_arr[i][2]], {
        icon: redIcon
      }).bindPopup(html).addTo(this.map);
    }
  }

  checkManhole(event: any) {
    if (event == 'A') {
      this.showManhole();
    } else {
      for (let i = 0; i < this.manhole_arr.length; i++) {
        this.map.removeLayer(this.manhole_marker[i]);
      }
    }
  }

  redirectHandler(path: any): void {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + path);
  }

  redirectHandler2(path: any): void {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cabinfo/' + path);
  }

}
