import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  currentUrl: string;
  allowMgmt: boolean = false;
  allowSearch: boolean = false

  constructor(
    private router: Router,
    private customer: CustomerService
  ) { }

  ngOnInit() {
    const group_pageaccess = JSON.parse(localStorage['group_pageaccess']);
    console.log(group_pageaccess);

    group_pageaccess.forEach((element: any) => {
      if (element.page_id == 1) {
        this.allowMgmt = true;
      }
    });

    // group_pageaccess.forEach((element: any) => {
    //   if (element.page_id == 3) {
    //     this.allowMgmt = true;
    //   }
    // });
  }

  logout() {
    this.customer.deleteToken();
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
