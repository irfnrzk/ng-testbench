import { DialogAlertModule } from './dialog-alert.module';

describe('DialogAlertModule', () => {
  let dialogAlertModule: DialogAlertModule;

  beforeEach(() => {
    dialogAlertModule = new DialogAlertModule();
  });

  it('should create an instance', () => {
    expect(dialogAlertModule).toBeTruthy();
  });
});
