import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogAlertComponent } from './dialog-alert.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule
  ],
  declarations: [
    DialogAlertComponent
  ],
  exports: [
    DialogAlertComponent
  ],
  entryComponents: [
    DialogAlertComponent
  ]
})
export class DialogAlertModule { }
