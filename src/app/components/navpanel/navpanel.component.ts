import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navpanel',
  templateUrl: './navpanel.component.html',
  styleUrls: ['./navpanel.component.scss']
})
export class NavpanelComponent implements OnInit {

  @Input() shape_id_data: any;

  // url param
  subscribedParam_w: any;
  subscribedParam_z: any;
  subscribedParam_n: any;
  subscribedParam_c: any;
  subscribedParam_ci: any;
  subscribedParam_d: any;
  subscribedParam_di: any;
  subscribedParam_si: any;

  network_element: string = "Level";
  location_desc: string = "Location";

  showWilayah = false;
  wilayah: string;
  showZone = false;
  zone: string;
  showNode = false;
  node: string;
  showCabinet = false;
  cabinet: string;
  showDp = false;
  dp: string;

  blacktab = true;
  showHelpButton = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
  }


  // onClickMe(event: Event ) {
  //   this.introJS.setOptions({
  //     steps: [
  //       {
  //         element: '#map-nav-1',
  //         intro: help.mapnav[0],
  //         position: 'right'
  //       }
  //     ]
  //   });
  //   this.introJS.start();
  // }

  black() {
    if (!this.blacktab) {
      return 'm-t-15 m-b-15 position-relative'
    } else {
      return 'tab-links-black'
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    let updatedVal = changes.shape_id_data.currentValue;


    // fetch url param
    this.subscribedParam_n = this.route.snapshot.paramMap.get('nodes');
    this.subscribedParam_w = this.route.snapshot.paramMap.get("wilayahs");
    this.subscribedParam_z = this.route.snapshot.paramMap.get("zones");
    this.subscribedParam_c = this.route.snapshot.paramMap.get("cabs");
    this.subscribedParam_ci = this.route.snapshot.paramMap.get("cabinfos");
    this.subscribedParam_d = this.route.snapshot.paramMap.get("dps");
    this.subscribedParam_di = this.route.snapshot.paramMap.get("netinfos");
    this.subscribedParam_si = this.route.snapshot.paramMap.get("serviceinfos");
    if (updatedVal) { this.PopulateMapNav(updatedVal) }

  }

  public PopulateMapNav(arg) {

    let updatedVal = arg;

    // Wilayah level
    if (this.subscribedParam_w !== undefined && this.subscribedParam_z == undefined) {

      this.blacktab = true;
      this.network_element = 'Wilayah';
      console.log(updatedVal.description);
      this.location_desc = updatedVal.description;
      this.showHelpButton = false;
    }
    // Zone level
    else if (this.subscribedParam_w !== undefined && this.subscribedParam_z !== undefined && this.subscribedParam_n == undefined) {

      this.blacktab = true;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);

      this.network_element = 'Zone';
      this.location_desc = updatedVal.zone[index].description;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      // this.showZone = true;
      // this.zone = updatedVal.zone[index].description;

    }
    // Node level
    else if (this.subscribedParam_w !== undefined && this.subscribedParam_z !== undefined && this.subscribedParam_c == undefined) {

      this.blacktab = true;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'TM NODE';
      this.location_desc = updatedVal.zone[index].node[index_w].description;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;

    }
    // Cabinet level
    else if (this.subscribedParam_w !== undefined && this.subscribedParam_z !== undefined && this.subscribedParam_c !== undefined && this.subscribedParam_d == undefined) {

      this.blacktab = true;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'Cabinet';
      this.location_desc = this.subscribedParam_c;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;
      this.showNode = true;
      this.node = updatedVal.zone[index].node[index_w].description;

    }
    // DP level
    else if (this.subscribedParam_w !== undefined && this.subscribedParam_z !== undefined && this.subscribedParam_c !== undefined && this.subscribedParam_d !== undefined) {

      this.blacktab = true;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'Dist. Point';
      this.location_desc = this.subscribedParam_d;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;
      this.showNode = true;
      this.node = updatedVal.zone[index].node[index_w].description;
      this.showCabinet = true;
      this.cabinet = this.subscribedParam_c;

    }

    // Cabinet info level
    if (this.subscribedParam_ci !== null) {
      // console.log(this.subscribedParam_di)

      this.blacktab = false;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'Cabinet';
      this.location_desc = this.subscribedParam_ci;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;
      this.showNode = true;
      this.node = updatedVal.zone[index].node[index_w].description;
      this.showHelpButton = false;

    }
    // Network info level
    else if (this.subscribedParam_di !== null) {
      console.log(this.subscribedParam_si)

      this.blacktab = false;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'Dist. Point';
      this.location_desc = this.subscribedParam_di;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;
      this.showNode = true;
      this.node = updatedVal.zone[index].node[index_w].description;
      this.showCabinet = true;
      this.cabinet = this.subscribedParam_c;
      this.showHelpButton = false;

    }
    // Pair info level
    else if (this.subscribedParam_c !== null && this.subscribedParam_d !== null && this.subscribedParam_si !== null) {

      this.blacktab = false;
      const index = updatedVal.zone.findIndex(item => item.slug === this.subscribedParam_z);
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.subscribedParam_n);

      this.network_element = 'Service No.';
      this.location_desc = this.subscribedParam_si;

      this.showWilayah = true;
      this.wilayah = updatedVal.description;
      this.showZone = true;
      this.zone = updatedVal.zone[index].description;
      this.showNode = true;
      this.node = updatedVal.zone[index].node[index_w].description;
      this.showCabinet = true;
      this.cabinet = this.subscribedParam_c;
      this.showDp = true;
      this.dp = this.subscribedParam_d;
      this.showHelpButton = false;
    }
  }

  returnToWilayah() {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w);
  }

  returnToZone() {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z);
  }

  returnToNode() {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n);
  }

  returnToCabinet() {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c);
  }

  returnToDp() {
    this.router.navigateByUrl('copper/wilayah/' + this.subscribedParam_w + '/zon/' + this.subscribedParam_z + '/node/' + this.subscribedParam_n + '/cab/' + this.subscribedParam_c + '/dp/' + this.subscribedParam_d);
  }


}
