import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavpanelComponent } from './navpanel.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    NavpanelComponent
  ],
  exports: [
    NavpanelComponent
  ]
})
export class NavpanelModule { }
