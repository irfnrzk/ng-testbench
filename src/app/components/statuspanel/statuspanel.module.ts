import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatuspanelComponent } from './statuspanel.component';
import { Ng2KnobDirective } from './angular2-knob.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    StatuspanelComponent,
    Ng2KnobDirective
  ],
  exports: [
    StatuspanelComponent
  ]
})
export class StatuspanelModule { }
