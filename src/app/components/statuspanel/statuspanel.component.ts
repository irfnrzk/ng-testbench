import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CabinetService } from 'src/app/services/cabinet.service';

@Component({
  selector: 'app-statuspanel',
  templateUrl: './statuspanel.component.html',
  styleUrls: ['./statuspanel.component.scss']
})
export class StatuspanelComponent implements OnInit {

  @Input() shape_id_data: any;
  @Input() total_network_element: number;
  @Input() cabinet_data: any;

  knOptions = {
    good: {
      readOnly: true,
      size: 60,
      unit: '',
      textColor: '#333030',
      fontWeigth: '700',
      max: 2,
      trackWidth: 5,
      barWidth: 4,
      trackColor: 'rgba(150, 195, 106, 0.3)',
      barColor: 'rgb(150, 195, 106)',
      dynamicOptions: true
    },
    caution: {
      readOnly: true,
      size: 60,
      unit: '',
      textColor: '#333030',
      fontWeigth: '700',
      max: 2,
      trackWidth: 5,
      barWidth: 4,
      trackColor: 'rgba(255,222,76,0.3)',
      barColor: '#ffde4c',
      dynamicOptions: true
    },
    unstable: {
      readOnly: true,
      size: 60,
      unit: '',
      textColor: '#333030',
      fontWeigth: '700',
      max: 2,
      trackWidth: 5,
      barWidth: 4,
      trackColor: 'rgba(255, 94, 94, 0.3)',
      barColor: 'rgb(255, 94, 94)',
      dynamicOptions: true
    },
    failure: {
      readOnly: true,
      size: 60,
      unit: '',
      textColor: '#333030',
      fontWeigth: '700',
      max: 2,
      trackWidth: 5,
      barWidth: 4,
      trackColor: '#b3b3b3',
      barColor: '#333030',
      dynamicOptions: true
    },
  }
  knValue = {
    good: 1,
    caution: 1,
    unstable: 1,
    failure: 1
  }
  network_element: string;
  coordinate: any;
  location_name: string;
  percentage: number = 0;
  state_status: string;
  imagePath: string;
  data: any;

  constructor(
    private route: ActivatedRoute,
    private cabinetService: CabinetService,
  ) { }

  ngOnInit() {
    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (viewportWidth > 1919) {
      this.knOptions.good.size = 120
      this.knOptions.caution.size = 120
      this.knOptions.unstable.size = 120
      this.knOptions.failure.size = 120
      this.knOptions.good.trackWidth = 10;
      this.knOptions.caution.trackWidth = 10;
      this.knOptions.unstable.trackWidth = 10;
      this.knOptions.failure.trackWidth = 10;
      this.knOptions.good.barWidth = 9;
      this.knOptions.caution.barWidth = 9;
      this.knOptions.unstable.barWidth = 9;
      this.knOptions.failure.barWidth = 9;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    let updatedVal = changes.shape_id_data.currentValue;
    if (updatedVal) { this.PopulateMapPanel(updatedVal) }
  }

  PopulateMapPanel(updatedVal: any) {

    // Wilayah Level
    if (this.route.snapshot.paramMap.get("zones") == undefined) {
      // console.log(updatedVal)
      this.network_element = "ZONE";
      this.location_name = updatedVal.name;
      this.percentage = updatedVal.percentage;
      this.state_status = updatedVal.status;
      this.imagePath = updatedVal.image_path;
      this.total_network_element = updatedVal.total_zone;
      this.updateKnob(
        updatedVal.total_zone,
        updatedVal.failure,
        updatedVal.unstable,
        updatedVal.caution,
        updatedVal.stable
      );
    }

    // Zone Level
    else if (this.route.snapshot.paramMap.get("nodes") == undefined) {
      this.network_element = "TM NODE";
      const index = updatedVal.zone.findIndex(item => item.slug === this.route.snapshot.paramMap.get("zones"));
      this.location_name = updatedVal.zone[index].description;
      this.percentage = updatedVal.zone[index].percentage;
      this.state_status = updatedVal.zone[index].status;
      this.imagePath = updatedVal.zone[index].image_path;
      this.updateKnob(
        updatedVal.zone[index].node.length,
        updatedVal.zone[index].failure,
        updatedVal.zone[index].unstable,
        updatedVal.zone[index].caution,
        updatedVal.zone[index].stable
      );
    }

    // Node Level
    else if (this.route.snapshot.paramMap.get("cabs") == undefined) {
      this.network_element = "CABINET";
      const index = updatedVal.zone.findIndex(item => item.slug === this.route.snapshot.paramMap.get("zones"));
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.route.snapshot.paramMap.get("nodes"));
      this.location_name = updatedVal.zone[index].node[index_w].name;
      this.coordinate = parseFloat(updatedVal.zone[index].node[index_w].lat).toFixed(6) + ', ' + parseFloat(updatedVal.zone[index].node[index_w].long).toFixed(6);
      this.percentage = updatedVal.zone[index].node[index_w].percentage;
      this.state_status = updatedVal.zone[index].node[index_w].status;
      let total_cabinets = updatedVal.zone[index].node[index_w].total_cabinet + updatedVal.zone[index].node[index_w].total_dslam + updatedVal.zone[index].node[index_w].total_msan + updatedVal.zone[index].node[index_w].total_rt
      this.updateKnob(
        total_cabinets,
        updatedVal.zone[index].node[index_w].failure,
        updatedVal.zone[index].node[index_w].unstable,
        updatedVal.zone[index].node[index_w].caution,
        updatedVal.zone[index].node[index_w].stable
      );

      if (updatedVal.zone[index].node[index_w].status == 'Good') {
        this.imagePath = '../assets/images/nodegood.png';
      } else if (updatedVal.zone[index].node[index_w].status == 'Caution') {
        this.imagePath = '../assets/images/nodecaution.png';
      } else if (updatedVal.zone[index].node[index_w].status == 'Unstable') {
        this.imagePath = '../assets/images/nodeunstable.png';
      } else {
        this.imagePath = '../assets/images/nodefailure.png';
      }
    }

    // Cabinet Level
    else if (this.route.snapshot.paramMap.get("dps") == undefined) {

      // console.log(this.cabinet_data);
      this.cabinetService.getDPbyCabinet(this.route.snapshot.paramMap.get("cabs")).subscribe(res => {
        this.total_network_element = res.data.length;
        console.log(this.total_network_element);
      });

      const index = updatedVal.zone.findIndex(item => item.slug === this.route.snapshot.paramMap.get("zones"));
      const index_w = updatedVal.zone[index].node.findIndex(item => item.slug === this.route.snapshot.paramMap.get("nodes"));
      let index_z;

      if (updatedVal.zone[index].node[index_w].cabinet.findIndex(item => item.cab_id === this.route.snapshot.paramMap.get("cabs")) == -1) {

        if (updatedVal.zone[index].node[index_w].msan.findIndex(item => item.msan_code === this.route.snapshot.paramMap.get("cabs")) == -1) {

          if (updatedVal.zone[index].node[index_w].dslam.findIndex(item => item.dslam_code === this.route.snapshot.paramMap.get("cabs")) == -1) {

            // RT
            index_z = updatedVal.zone[index].node[index_w].rt.findIndex(item => item.rt_code === this.route.snapshot.paramMap.get("cabs"));
            this.location_name = updatedVal.zone[index].node[index_w].rt[index_z].rt_code;
            this.percentage = updatedVal.zone[index].node[index_w].rt[index_z].percentage;
            this.state_status = updatedVal.zone[index].node[index_w].rt[index_z].status;
            this.coordinate = parseFloat(updatedVal.zone[index].node[index_w].rt[index_z].lat).toFixed(6) + ', ' + parseFloat(updatedVal.zone[index].node[index_w].rt[index_z].long).toFixed(6);

            if (updatedVal.zone[index].node[index_w].rt[index_z].status == 'Good') {
              this.imagePath = '../assets/images/cabinetgood.png';
            } else if (updatedVal.zone[index].node[index_w].rt[index_z].status == 'Caution') {
              this.imagePath = '../assets/images/cabinetcaution.png';
            } else if (updatedVal.zone[index].node[index_w].rt[index_z].status == 'Unstable') {
              this.imagePath = '../assets/images/cabinetunstable.png';
            } else {
              this.imagePath = '../assets/images/cabinetfailure.png';
            }

          } else {

            // DSLAM
            index_z = updatedVal.zone[index].node[index_w].dslam.findIndex(item => item.dslam_code === this.route.snapshot.paramMap.get("cabs"));
            this.location_name = updatedVal.zone[index].node[index_w].dslam[index_z].dslam_code;
            this.percentage = updatedVal.zone[index].node[index_w].dslam[index_z].percentage;
            this.state_status = updatedVal.zone[index].node[index_w].dslam[index_z].status;
            this.coordinate = parseFloat(updatedVal.zone[index].node[index_w].dslam[index_z].lat).toFixed(6) + ', ' + parseFloat(updatedVal.zone[index].node[index_w].dslam[index_z].long).toFixed(6);

            if (updatedVal.zone[index].node[index_w].dslam[index_z].status == 'Good') {
              this.imagePath = '../assets/images/cabinetgood.png';
            } else if (updatedVal.zone[index].node[index_w].dslam[index_z].status == 'Caution') {
              this.imagePath = '../assets/images/cabinetcaution.png';
            } else if (updatedVal.zone[index].node[index_w].dslam[index_z].status == 'Unstable') {
              this.imagePath = '../assets/images/cabinetunstable.png';
            } else {
              this.imagePath = '../assets/images/cabinetfailure.png';
            }
          }

        } else {

          // MSAN
          index_z = updatedVal.zone[index].node[index_w].msan.findIndex(item => item.msan_code === this.route.snapshot.paramMap.get("cabs"));
          this.location_name = updatedVal.zone[index].node[index_w].msan[index_z].msan_code;
          this.percentage = updatedVal.zone[index].node[index_w].msan[index_z].percentage;
          this.state_status = updatedVal.zone[index].node[index_w].msan[index_z].status;
          this.coordinate = parseFloat(updatedVal.zone[index].node[index_w].msan[index_z].lat).toFixed(6) + ', ' + parseFloat(updatedVal.zone[index].node[index_w].msan[index_z].long).toFixed(6);

          if (updatedVal.zone[index].node[index_w].msan[index_z].status == 'Good') {
            this.imagePath = '../assets/images/cabinetgood.png';
          } else if (updatedVal.zone[index].node[index_w].msan[index_z].status == 'Caution') {
            this.imagePath = '../assets/images/cabinetcaution.png';
          } else if (updatedVal.zone[index].node[index_w].msan[index_z].status == 'Unstable') {
            this.imagePath = '../assets/images/cabinetunstable.png';
          } else {
            this.imagePath = '../assets/images/cabinetfailure.png';
          }

        }

      } else {

        // Cabinet
        index_z = updatedVal.zone[index].node[index_w].cabinet.findIndex(item => item.cab_id === this.route.snapshot.paramMap.get("cabs"));
        this.location_name = updatedVal.zone[index].node[index_w].cabinet[index_z].cab_id;
        this.percentage = updatedVal.zone[index].node[index_w].cabinet[index_z].percentage;
        this.state_status = updatedVal.zone[index].node[index_w].cabinet[index_z].status;
        this.coordinate = parseFloat(updatedVal.zone[index].node[index_w].cabinet[index_z].lat).toFixed(6) + ', ' + parseFloat(updatedVal.zone[index].node[index_w].cabinet[index_z].long).toFixed(6);

        if (updatedVal.zone[index].node[index_w].cabinet[index_z].status == 'Good') {
          this.imagePath = '../assets/images/cabinetgood.png';
        } else if (updatedVal.zone[index].node[index_w].cabinet[index_z].status == 'Caution') {
          this.imagePath = '../assets/images/cabinetcaution.png';
        } else if (updatedVal.zone[index].node[index_w].cabinet[index_z].status == 'Unstable') {
          this.imagePath = '../assets/images/cabinetunstable.png';
        } else {
          this.imagePath = '../assets/images/cabinetfailure.png';
        }
      }

      this.network_element = "DP";

    }

    // DP Level
    else if (this.route.snapshot.paramMap.get("cabs") !== undefined && this.route.snapshot.paramMap.get("dps") !== undefined) {

      this.network_element = "DP Pair";
      this.cabinetService.getDPbyCabinet(this.route.snapshot.paramMap.get("cabs")).subscribe(res => {
        const index = res.data.findIndex(item => item.dp_id === this.route.snapshot.paramMap.get("dps"));
        this.location_name = res.data[index].dp_id;
        this.coordinate = parseFloat(res.data[index].lat).toFixed(6) + ', ' + parseFloat(res.data[index].long).toFixed(6);
        this.state_status = res.data[index].status;
        this.total_network_element = res.data[index].no_of_service;

        if (res.data[index].status == '1') {
          this.imagePath = '../assets/images/dpgood.png';
        } else if (res.data[index].status == '2') {
          this.imagePath = '../assets/images/dpcaution.png';
        } else if (res.data[index].status == '2') {
          this.imagePath = '../assets/images/dpunstable.png';
        } else {
          this.imagePath = '../assets/images/dpfailure.png';
        }

      });
    }
  }

  updateKnob(total: number, failure: number, unstable: number, caution: number, good: number) {
    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    this.knValue.good = good;
    if (viewportWidth > 1919) {
      this.knOptions.good = {
        readOnly: true,
        size: 120,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 10,
        barWidth: 9,
        trackColor: 'rgba(150, 195, 106, 0.3)',
        barColor: 'rgb(150, 195, 106)',
        dynamicOptions: true
      };
    } else {
      this.knOptions.good = {
        readOnly: true,
        size: 60,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 5,
        barWidth: 4,
        trackColor: 'rgba(150, 195, 106, 0.3)',
        barColor: 'rgb(150, 195, 106)',
        dynamicOptions: true
      };
    }

    this.knValue.caution = caution;
    if (viewportWidth > 1919) {
      this.knOptions.caution = {
        readOnly: true,
        size: 120,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 10,
        barWidth: 9,
        trackColor: 'rgba(255,222,76,0.3)',
        barColor: '#ffde4c',
        dynamicOptions: true
      };
    } else {
      this.knOptions.caution = {
        readOnly: true,
        size: 60,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 5,
        barWidth: 4,
        trackColor: 'rgba(255,222,76,0.3)',
        barColor: '#ffde4c',
        dynamicOptions: true
      };
    }

    this.knValue.unstable = unstable;
    if (viewportWidth > 1919) {
      this.knOptions.unstable = {
        readOnly: true,
        size: 120,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 10,
        barWidth: 9,
        trackColor: 'rgba(255, 94, 94, 0.3)',
        barColor: 'rgb(255, 94, 94)',
        dynamicOptions: true
      };
    } else {
      this.knOptions.unstable = {
        readOnly: true,
        size: 60,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 5,
        barWidth: 4,
        trackColor: 'rgba(255, 94, 94, 0.3)',
        barColor: 'rgb(255, 94, 94)',
        dynamicOptions: true
      };
    }

    this.knValue.failure = failure;
    if (viewportWidth > 1919) {
      this.knOptions.failure = {
        readOnly: true,
        size: 120,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 10,
        barWidth: 9,
        trackColor: '#b3b3b3',
        barColor: '#333030',
        dynamicOptions: true
      };
    } else {
      this.knOptions.failure = {
        readOnly: true,
        size: 60,
        unit: '',
        textColor: '#333030',
        fontWeigth: '700',
        max: total,
        trackWidth: 5,
        barWidth: 4,
        trackColor: '#b3b3b3',
        barColor: '#333030',
        dynamicOptions: true
      };
    }
  }

}