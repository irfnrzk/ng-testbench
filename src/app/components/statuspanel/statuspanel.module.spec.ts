import { StatuspanelModule } from './statuspanel.module';

describe('StatuspanelModule', () => {
  let statuspanelModule: StatuspanelModule;

  beforeEach(() => {
    statuspanelModule = new StatuspanelModule();
  });

  it('should create an instance', () => {
    expect(statuspanelModule).toBeTruthy();
  });
});
