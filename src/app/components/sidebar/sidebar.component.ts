import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  drawIn: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  drawerIn() {
    this.drawIn = !this.drawIn;
  }

}
