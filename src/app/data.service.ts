import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of, forkJoin } from 'rxjs';
import { map, catchError, share } from "rxjs/operators";
import { Endpoints } from "./endpoint"

@Injectable({
  providedIn: 'root'
})
export class DataService {

  data: any;
  observable: any;
  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  // App login check via LDAP Authentication
  loginOip(credentials: any) {
    let headers = new HttpHeaders({
      "Authorization": "Bearer 885cc8c3-b957-32f8-a92e-83a739ca2f92",
      "Content-Type": "application/json"
    });
    console.log("https://api.oip.tm.com.my/app/t/tmrnd.com.my/ldapishield/1.0/auth", JSON.stringify(credentials), { headers: headers })
    return this.http
      .post("https://api.oip.tm.com.my/app/t/tmrnd.com.my/ldapishield/1.0/auth", JSON.stringify(credentials), { headers: headers })
    // .pipe(
    //   map((response: Response) => response),
    //   catchError(error => {
    //     return Observable.throw(error);
    //   })
    // );
  }

  // dylia/api/inventory/get_wilayah_view
  getOverallView(): Observable<any> {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/inventory/get_wilayah_view").pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  // Get Element View & Cache
  getElementView(id: string) {
    if (this.data) {
      if (this.data.data[0].shape_id == id) {
        return of(this.data);
      } else {
        this.observable = this.http.get(this.DB_ENDPOINT + "dylia/get_element_view_service?wilayah_id=" + id, { observe: 'response' })
          .pipe(
            map((response: Response) => {
              this.observable = null;
              if (response.status === 400) {
                return 'Request failed.';
              } else if (response.status === 200) {
                this.data = response.body;
                console.log(this.data);
                return this.data;
              }
            }),
            catchError(error => {
              return Observable.throw(error);
            })
          )
          .pipe(share());
        return this.observable;
      }
    } else if (this.observable) {
      return this.observable;
    } else {
      this.observable = this.http.get(this.DB_ENDPOINT + "dylia/get_element_view_service?wilayah_id=" + id, { observe: 'response' })
        .pipe(
          map((response: Response) => {
            this.observable = null;
            if (response.status === 400) {
              return 'Request failed.';
            } else if (response.status === 200) {
              this.data = response.body;
              // console.log(this.data);
              return this.data;
            }
          }),
          catchError(error => {
            return Observable.throw(error);
          })
        )
        .pipe(share());
      return this.observable;
    }
  }

  getDPPairbyDP(d): any {
    // return this.http.get("../assets/demo_data/dp_pair_id=" + d + ".json").pipe(
    return this.http.get(this.DB_ENDPOINT + "dylia/dp_pair_id?dp_id=" + d).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

}
