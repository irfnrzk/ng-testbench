import { TestBed } from '@angular/core/testing';

import { NetworkinfoService } from './networkinfo.service';

describe('NetworkinfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NetworkinfoService = TestBed.get(NetworkinfoService);
    expect(service).toBeTruthy();
  });
});
