import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { map, filter, switchMap, catchError, observeOn, share } from "rxjs/operators";
import { of, Observable, forkJoin, BehaviorSubject } from "rxjs";
import { Endpoints } from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class CabinetinfoService {

  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  data: any;

  private mySubject = new BehaviorSubject<any>([]);
  allObservable: Observable<any> = this.mySubject.asObservable();

  setUpdatedData(string) {
    this.mySubject.next(string);
  }

  //Cable Bundle info
  cabinetInfo(c): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/cable_bundle_info?cab_id=" + c).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //Cabinet Details Info
  getCabinetBundle(c, b): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashbaord/cable_info?cab_id=" + c + "&cable_id=" + b).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //Fake json for testing
  // getCabinetBundle(c, b): any {
  //   return this.http.get("/src/assets/demo_data/cab_info_new.json").pipe(
  //     map((response: Response) => response),
  //     catchError(error => {
  //       return Observable.throw(error);
  //     })
  //   );
  // }

  getCabinetBundleLT(c, b): any {
    return this.http.get(this.DB_ENDPOINT + "user/getCabinet?cab_id=" + c + "&cable_id=" + b).pipe(
      // return this.http.get(this.DB_ENDPOINT + "dylia/cable_info_detail?cab_id=" + c + "&cable_id=" + b).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //get NTT
  getNTT(c): any {
    return this.http.get("assets/demo_data/service_info_ctt.json").pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

}
