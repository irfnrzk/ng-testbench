import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Endpoints } from '../endpoint';
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class CabinetService {

  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  getDPbyCabinet(c: string): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/dp_list_summary?cab_id=" + c).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }
}