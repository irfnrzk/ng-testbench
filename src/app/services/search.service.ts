import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Http, Response } from "@angular/http";
import { map, catchError } from "rxjs/operators";
import { Observable } from "rxjs";
import { Endpoints } from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  DB_ENDPOINT = Endpoints.DB;
  DB_ENDPOINT_TEST = "http://10.44.5.38:1880/"

  constructor(private http: HttpClient) { }

  getWilayah() {
    var url = this.DB_ENDPOINT + 'dylia/api/user/cabinet_all_wilayah';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getCabinetId(cabinetId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/cabinet?cabinet_id=' + cabinetId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getDpId(dpId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/dp?dp_id=' + dpId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getServiceId(serviceId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/service?service_id=' + serviceId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getWilayahAll() {
    var url = this.DB_ENDPOINT + 'dylia/api/user/cabinet_all_wilayahB';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getCabIdAll(cabId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/query_cabinet_id?cab_id=' + cabId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getDpIdAll(dpId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/query_dp_id?dp_id=' + dpId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getSvcIdAll(svcId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/query_service_number?service_number=' + svcId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getDpPairIdAll(dpPairId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/query_dp_pair_id?dp_pair_id=' + dpPairId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getUnifiId(unifiId: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/query_unifi_id?unifi_id=' + unifiId;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getAllZones() {
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/query_all_zone';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }
}
