import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Response } from "@angular/http";
import { map, catchError } from "rxjs/operators";
import { Observable } from "rxjs";
import { Endpoints } from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class ServiceinfoService {

  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  //Service Details Info
  getServiceInfo(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/service_info?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //Fake json for testing
  // getServiceInfo(e): any {
  //   return this.http.get("/src/assets/demo_data/service_info_new.json").pipe(
  //     map((response: Response) => response),
  //     catchError(error => {
  //       return Observable.throw(error);
  //     })
  //   );
  // }

  //Service Info after line test
  getServiceInfoLT(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/user/getServiceInfoDetails?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //Graph & Results
  getGraphSNR(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/snr_graph?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getGraphAtt(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/sigAtt-graph?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getGraphAttR(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/maxAttain_graph?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getGraphIR(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/ir?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getGraphCR(e): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/cr?dp_pair_id=" + e).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getCTT(e): any {
    return this.http.get("assets/mock_data/service_info_ctt.json").pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }
}
