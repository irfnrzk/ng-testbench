import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { map, filter, switchMap, catchError, observeOn, share } from "rxjs/operators";
import { of, Observable, forkJoin } from "rxjs";
import { Endpoints } from '../endpoint';


@Injectable({
  providedIn: 'root'
})
export class NetworkinfoService {

  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  networkInfo(c): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/dashboard/network_detail?dp_id=" + c).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //Fake json for testing
  // networkInfo(c): any {
  //   return this.http.get("/src/assets/demo_data/network_info_new.json").pipe(
  //     map((response: Response) => response),
  //     catchError(error => {
  //       return Observable.throw(error);
  //     })
  //   );
  // }

  //Network Info after line test
  networkInfoLT(dpID): any {
    return this.http.get(this.DB_ENDPOINT + "dylia/api/user/getNetwork?dp_id=" + dpID).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  //get NTT
  getNTT(d): any {
    return this.http.get("assets/demo_data/service_info_ctt.json").pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }
}
