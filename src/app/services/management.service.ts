import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { map, catchError } from "rxjs/operators";
import { Observable } from "rxjs";
import { Endpoints } from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  DB_ENDPOINT = Endpoints.DB;
  DB_ENDPOINT_TEST: string = "http://10.44.5.38:1880/"

  constructor(private http: HttpClient, private httpAng: Http) { }

  getUser() {
    var url = this.DB_ENDPOINT + 'dylia/api/user/all_user';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getUserIndividual(staffid: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/get_user?staff_id=' + staffid;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getUserGroup(groupid: any) {
    var url = this.DB_ENDPOINT + 'dylia/api/user/get_user_group?group_id=' + groupid;
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getGroup() {
    var url = this.DB_ENDPOINT + 'dylia/api/user/all_group';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getAccessPage() {
    var url = this.DB_ENDPOINT + 'dylia/api/user/all_pages';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getDistrictList() {
    var url = '../assets/mock_data/districtlist.json';
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  changeStatus(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_user_status';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  createUser(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/add_user';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  editUser(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_user';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  addStaffPages(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/user/add_user_staff_page';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  updateLastLogin(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/update_last_login';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  createGroup(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/add_group';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  editAccess(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_group';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  editGroup(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_group_only';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  editDefaultPage(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_default_page';
    return this.httpAng
      .post(url, JSON.stringify(data), { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  deleteUser(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/del_user';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  deleteGroup(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/del_group';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  editPages(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT + 'dylia/api/user/edit_page';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  getSchedule() {
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/all_schedulers'
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  getScheduleInfo(id: string) {
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/get_scheduler_info?sched_id=' + id
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  submitZonePriority(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/set_scheduler_zone';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  addSchedule(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/add_scheduler_autoid';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  deleteSchedule(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/del_scheduler';
    return this.httpAng
      .post(url, data, { headers: headers })
      .pipe(
        map((response: Response) => response),
        catchError(error => {
          return Observable.throw(error);
        })
      );
  }

  systemHealth() {
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/query_system_health'
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }

  checkSysHealth() {
    var url = this.DB_ENDPOINT_TEST + 'dylia/api/system/system_health_check'
    return this.http.get(url).pipe(
      map((response: Response) => response),
      catchError(error => {
        return Observable.throw(error);
      })
    );
  }
}