import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, forkJoin } from 'rxjs';
import { Endpoints } from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  DB_ENDPOINT = Endpoints.DB;

  constructor(private http: HttpClient) { }

  // Get NE at Node
  NodeApi(cabinet: string): Observable<any[]> {
    let response1 = this.http.get(this.DB_ENDPOINT + "dylia/inv_manhole?exc_abb=" + cabinet);
    let response2 = this.http.get(this.DB_ENDPOINT + "dylia/inv_pole?exc_abb=" + cabinet);
    let response3 = this.http.get(this.DB_ENDPOINT + "dylia/inv_dp?exc_abb=" + cabinet);
    let response4 = this.http.get(this.DB_ENDPOINT + "dylia/get_exch_area?exch_id=" + cabinet); // boundary

    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return forkJoin([response1, response2, response3, response4]);
  }

  // Get Cable Data

}
