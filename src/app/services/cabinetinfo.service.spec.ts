import { TestBed } from '@angular/core/testing';

import { CabinetinfoService } from './cabinetinfo.service';

describe('CabinetinfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CabinetinfoService = TestBed.get(CabinetinfoService);
    expect(service).toBeTruthy();
  });
});
