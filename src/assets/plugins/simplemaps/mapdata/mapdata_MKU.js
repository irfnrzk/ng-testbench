var simplemaps_countrymap_mapdata = {
  main_settings: {
    //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",

    //State defaults
    state_description: "State description",
    state_color: "#808080",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 0.5,
    all_states_inactive: "no",
    all_states_zoomable: "no",

    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",

    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",

    //Zoom settings
    zoom: "yes",
    manual_zoom: "no",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,

    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",

    //Advanced settings
    div: "zmap",
    auto_load: "no",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    ABU: {
      name: "Ampang Batu",
      color: "#96C36A"
    },
    AG: {
      name: "Alor Gajah",
      color: "#96C36A"
    },
    BBM: {
      name: "Batu Berendam",
      color: "#96C36A"
    },
    BTK: {
      name: "Bukit Terendak",
      color: "#96C36A"
    },
    BTM: {
      name: "Bertam",
      color: "#96C36A"
    },
    DT: {
      name: "Durian Tunggal",
      color: "#96C36A"
    },
    KEL: {
      name: "Kelebang",
      color: "#96C36A"
    },
    KSB: {
      name: "Kuala Sungai Baru",
      color: "#96C36A"
    },
    LCA: {
      name: "Lubuk Cina",
      color: "#96C36A"
    },
    MAM: {
      name: "Malim",
      color: "#96C36A"
    },
    MJ: {
      name: "Masjid Tanah",
      color: "#96C36A"
    },
    RBA: {
      name: "Rumbia",
      color: "#96C36A"
    },
    SEM: {
      name: "Simpang Empat",
      color: "#96C36A"
    },
    TB: {
      name: "Tebong",
      color: "#96C36A"
    },
    TGB: {
      name: "Tangga Batu",
      color: "#96C36A"
    },
    MKS: {
      name: "Melaka Selatan",
      color: "#808080"
    },
    JOHOR: {
      name: "Johor",
      color: "#808080"
    },
    NSS: {
      name: "Zone NS Selatan",
      color: "#808080"
    },
    NST: {
      name: "Zone NS Timur",
      color: "#808080"
    }
  },
  locations: {},
  labels: {},
  regions: {}
};