var simplemaps_countrymap_mapdata = {
  main_settings: {
    //General settings
    width: "responsive", //'700' or 'responsive'
    // background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",

    //State defaults
    state_description: "",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 0.2,
    all_states_inactive: "no",
    all_states_zoomable: "no",

    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",

    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",

    //Zoom settings
    zoom: "yes",
    manual_zoom: "no",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,

    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",

    //Advanced settings
    div: "petamalaysia",
    auto_load: "no",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    MYS1000: {
      name: "Johor",
      color: "#F85B5B"
    },
    MIRIBINTULU: {
      name: "Miri Bintulu",
      color: "#F85B5B"
    },
    SIBUSRIAMAN: {
      name: "Sibu Sri Aman",
      color: "#F85B5B"
    },
    KUCHINGSTAMPIN: {
      name: "Kuching Stampin",
      color: "#F85B5B"
    },
    NSEMBILAN: {
      name: "Negeri Sembilan",
      color: "#96C36A"
    },
    MELAKA: {
      name: "Melaka",
      color: "#96C36A"
    },
    MYS5000: {
      name: "Pahang",
      color: "#F85B5B"
    },
    KELANTAN: {
      name: "Kelantan",
      color: "#F85B5B"
    },
    MYS7000: {
      name: "Terengganu",
      color: "#F85B5B"
    },
    MYS9000: {
      name: "Selangor",
      color: "#F85B5B"
    },
    MYS10000: {
      name: "Kuala Lumpur",
      color: "#F85B5B"
    },
    MYS11000: {
      name: "Kedah/Perlis",
      color: "#F85B5B"
    },
    MYS12000: {
      name: "Penang",
      color: "#F85B5B"
    },
    MYS13000: {
      name: "Perak",
      color: "#F85B5B"
    },
    KKINABALU: {
      name: "Kota Kinabalu",
      color: "#F85B5B"
    },
    PBPLABUAN: {
      name: "PBP & Labuan",
      color: "#F85B5B"
    },
    SANDTWULHD: {
      name: "Sandakan, Tawau & Lahad Datu",
      color: "#F85B5B"
    }
  },
  locations: {},
  labels: {},
  regions: {}
};