var simplemaps_countrymap_mapdata = {
  main_settings: {
    //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",

    //State defaults
    state_description: "State description",
    state_color: "#96C36A",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 0.5,
    all_states_inactive: "no",
    all_states_zoomable: "no",

    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",

    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",

    //Zoom settings
    zoom: "yes",
    manual_zoom: "no",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,

    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",

    //Advanced settings
    div: "zmap",
    auto_load: "no",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    AHN: {
      name: "Air Hitam (NS)",
      color: "#96C36A"
    },
    AKG: {
      name: "Air Kuning (NS)",
      color: "#96C36A"
    },
    BH: {
      name: "Bahau",
      color: "#96C36A"
    },
    BIR: {
      name: "Batu Kikir",
      color: "#96C36A"
    },
    BX: {
      name: "Btg Melaka",
      color: "#96C36A"
    },
    FSH: {
      name: "Felda Pasoh 1",
      color: "#96C36A"
    },
    FSO: {
      name: "Felda Pasoh 3",
      color: "#96C36A"
    },
    GCH: {
      name: "Gemencheh",
      color: "#96C36A"
    },
    GS: {
      name: "Gemas",
      color: "#96C36A"
    },
    JAI: {
      name: "Jelai",
      color: "#96C36A"
    },
    JL: {
      name: "Johol",
      color: "#96C36A"
    },
    KES: {
      name: "Kepis",
      color: "#96C36A"
    },
    KP: {
      name: "Kuala Pilah",
      color: "#96C36A"
    },
    LGS: {
      name: "Ladang Geddes",
      color: "#96C36A"
    },
    LMA: {
      name: "Lui Muda",
      color: "#96C36A"
    },
    PAT: {
      name: "Palong Tiga",
      color: "#96C36A"
    },
    PLD: {
      name: "Palong Dua",
      color: "#96C36A"
    },
    PLL: {
      name: "Palong Lapan",
      color: "#96C36A"
    },
    PLM: {
      name: "Palong Lima",
      color: "#96C36A"
    },
    PLS: {
      name: "Palong Satu",
      color: "#96C36A"
    },
    PTG: {
      name: "Pertang",
      color: "#96C36A"
    },
    RN: {
      name: "Rompin",
      color: "#96C36A"
    },
    SDN: {
      name: "Simpang Durian",
      color: "#96C36A"
    },
    SHR: {
      name: "Serting Hilir",
      color: "#96C36A"
    },
    SMI: {
      name: "Sri Menanti",
      color: "#96C36A"
    },
    SPT: {
      name: "Simpang Pertang",
      color: "#96C36A"
    },
    SRT: {
      name: "Serting",
      color: "#96C36A"
    },
    TCI: {
      name: "Terachi",
      color: "#96C36A"
    }
  },
  locations: {},
  labels: {},
  regions: {}
};